# @solarschools/education-sdk

SolarSchools - JavaScript client for @solarschools/education-sdk
No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
This SDK is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: 0.0.1
- Package version: 0.0.0
- Build package: org.openapitools.codegen.languages.JavascriptClientCodegen

## Installation

### For [Node.js](https://nodejs.org/)

#### npm

To publish the library as a [npm](https://www.npmjs.com/), please follow the procedure in ["Publishing npm packages"](https://docs.npmjs.com/getting-started/publishing-npm-packages).

Then install it via:

```shell
npm install @solarschools/education-sdk --save
```

Finally, you need to build the module:

```shell
npm run build
```

##### Local development

To use the library locally without publishing to a remote npm registry, first install the dependencies by changing into the directory containing `package.json` (and this README). Let's call this `JAVASCRIPT_CLIENT_DIR`. Then run:

```shell
npm install
```

Next, [link](https://docs.npmjs.com/cli/link) it globally in npm with the following, also from `JAVASCRIPT_CLIENT_DIR`:

```shell
npm link
```

To use the link you just defined in your project, switch to the directory you want to use your @solarschools/education-sdk from, and run:

```shell
npm link /path/to/<JAVASCRIPT_CLIENT_DIR>
```

Finally, you need to build the module:

```shell
npm run build
```

#### git

If the library is hosted at a git repository, e.g.https://github.com/GIT_USER_ID/GIT_REPO_ID
then install it via:

```shell
    npm install GIT_USER_ID/GIT_REPO_ID --save
```

### For browser

The library also works in the browser environment via npm and [browserify](http://browserify.org/). After following
the above steps with Node.js and installing browserify with `npm install -g browserify`,
perform the following (assuming *main.js* is your entry file):

```shell
browserify main.js > bundle.js
```

Then include *bundle.js* in the HTML pages.

### Webpack Configuration

Using Webpack you may encounter the following error: "Module not found: Error:
Cannot resolve module", most certainly you should disable AMD loader. Add/merge
the following section to your webpack config:

```javascript
module: {
  rules: [
    {
      parser: {
        amd: false
      }
    }
  ]
}
```

## Getting Started

Please follow the [installation](#installation) instruction and execute the following JS code:

```javascript
var SolarSchools = require('@solarschools/education-sdk');


var api = new SolarSchools.AuthenticationApi()
var opts = {
  'login': new SolarSchools.Login() // {Login} Payload for logging in a user.
};
api.login(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});


```

## Documentation for API Endpoints

All URIs are relative to *https://api.solarschools.net/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*SolarSchools.AuthenticationApi* | [**login**](docs/AuthenticationApi.md#login) | **POST** /authentication/login | 
*SolarSchools.AuthenticationApi* | [**logout**](docs/AuthenticationApi.md#logout) | **POST** /authentication/logout | 
*SolarSchools.AuthenticationApi* | [**refreshSession**](docs/AuthenticationApi.md#refreshSession) | **POST** /authentication/refresh | 
*SolarSchools.AuthenticationApi* | [**testAccess**](docs/AuthenticationApi.md#testAccess) | **GET** /authentication/access-test | 
*SolarSchools.ContractorsApi* | [**callDelete**](docs/ContractorsApi.md#callDelete) | **DELETE** /contractors/{id} | 
*SolarSchools.ContractorsApi* | [**create**](docs/ContractorsApi.md#create) | **POST** /contractors/ | 
*SolarSchools.ContractorsApi* | [**get**](docs/ContractorsApi.md#get) | **GET** /contractors/{id} | 
*SolarSchools.ContractorsApi* | [**list**](docs/ContractorsApi.md#list) | **GET** /contractors/ | 
*SolarSchools.ContractorsApi* | [**update**](docs/ContractorsApi.md#update) | **PUT** /contractors/{id} | 
*SolarSchools.DataApi* | [**getChildEnergyBreakdown**](docs/DataApi.md#getChildEnergyBreakdown) | **GET** /data/{resourceType}/{resourceId}/energy/child-breakdown | 
*SolarSchools.DataApi* | [**getChildEnergyStats**](docs/DataApi.md#getChildEnergyStats) | **GET** /data/{resourceType}/{resourceId}/energy/child-stats | 
*SolarSchools.DataApi* | [**getChildSolarStats**](docs/DataApi.md#getChildSolarStats) | **GET** /data/{resourceType}/{resourceId}/solar/child-stats | 
*SolarSchools.DataApi* | [**getComparison**](docs/DataApi.md#getComparison) | **GET** /data/{resourceType}/{resourceId}/compare/{property} | 
*SolarSchools.DataApi* | [**getEmissionsFeed**](docs/DataApi.md#getEmissionsFeed) | **GET** /data/{resourceType}/{resourceId}/emissions/feed | 
*SolarSchools.DataApi* | [**getEnergyFeed**](docs/DataApi.md#getEnergyFeed) | **GET** /data/{resourceType}/{resourceId}/energy/feed | 
*SolarSchools.DataApi* | [**getEnergyPredictions**](docs/DataApi.md#getEnergyPredictions) | **GET** /data/{resourceType}/{resourceId}/energy/predictions | 
*SolarSchools.DataApi* | [**getEnergyStats**](docs/DataApi.md#getEnergyStats) | **GET** /data/{resourceType}/{resourceId}/energy/stats | 
*SolarSchools.DataApi* | [**getSiteEventEnergyFeed**](docs/DataApi.md#getSiteEventEnergyFeed) | **GET** /data/sites/{siteId}/events/{eventId}/energy/feed | 
*SolarSchools.DataApi* | [**getSitesEnergyRanking**](docs/DataApi.md#getSitesEnergyRanking) | **GET** /data/{resourceType}/{resourceId}/sites/energy/ranking | 
*SolarSchools.DataApi* | [**getSummary**](docs/DataApi.md#getSummary) | **GET** /data/{resourceType}/{resourceId}/summary | 
*SolarSchools.DataApi* | [**getWaterFeed**](docs/DataApi.md#getWaterFeed) | **GET** /data/{resourceType}/{resourceId}/water/feed | 
*SolarSchools.DataApi* | [**getWaterStats**](docs/DataApi.md#getWaterStats) | **GET** /data/{resourceType}/{resourceId}/water/stats | 
*SolarSchools.OrganisationsApi* | [**get**](docs/OrganisationsApi.md#get) | **GET** /organisations/{id} | 
*SolarSchools.OrganisationsApi* | [**getMapSites**](docs/OrganisationsApi.md#getMapSites) | **GET** /organisations/{id}/sites/map | 
*SolarSchools.OrganisationsApi* | [**getSchemas**](docs/OrganisationsApi.md#getSchemas) | **GET** /organisations/{id}/schemas | 
*SolarSchools.OrganisationsApi* | [**getSiteCustomAttributes**](docs/OrganisationsApi.md#getSiteCustomAttributes) | **GET** /organisations/{id}/sites/attributes | 
*SolarSchools.OrganisationsApi* | [**getSites**](docs/OrganisationsApi.md#getSites) | **GET** /organisations/{id}/sites | 
*SolarSchools.OrganisationsApi* | [**getSolarCurrentStats**](docs/OrganisationsApi.md#getSolarCurrentStats) | **GET** /organisations/{id}/solar/stats/current | 
*SolarSchools.OrganisationsApi* | [**getSummary**](docs/OrganisationsApi.md#getSummary) | **GET** /organisations/{id}/summary | 
*SolarSchools.OrganisationsApi* | [**testSiteName**](docs/OrganisationsApi.md#testSiteName) | **GET** /organisations/{id}/sites/test-name | 
*SolarSchools.SchemasApi* | [**get**](docs/SchemasApi.md#get) | **GET** /schemas/{id} | 
*SolarSchools.SchemasApi* | [**getContractors**](docs/SchemasApi.md#getContractors) | **GET** /schemas/{id}/contractors | 
*SolarSchools.SchemasApi* | [**getEnergyBreakdown**](docs/SchemasApi.md#getEnergyBreakdown) | **GET** /schemas/{id}/energy-breakdown | 
*SolarSchools.SchemasApi* | [**getEnergyDataFeed**](docs/SchemasApi.md#getEnergyDataFeed) | **GET** /schemas/{id}/energy/data-feed | 
*SolarSchools.SchemasApi* | [**getEnergyLifetimeSummary**](docs/SchemasApi.md#getEnergyLifetimeSummary) | **GET** /schemas/{id}/energy/summary/lifetime | 
*SolarSchools.SchemasApi* | [**getList**](docs/SchemasApi.md#getList) | **GET** /schemas/ | 
*SolarSchools.SchemasApi* | [**getMapSites**](docs/SchemasApi.md#getMapSites) | **GET** /schemas/{id}/sites/map | 
*SolarSchools.SchemasApi* | [**getResourceBySlug**](docs/SchemasApi.md#getResourceBySlug) | **GET** /schemas/{id}/resources/{slug} | 
*SolarSchools.SchemasApi* | [**getSites**](docs/SchemasApi.md#getSites) | **GET** /schemas/{id}/sites | 
*SolarSchools.SchemasApi* | [**getSolarCurrentStats**](docs/SchemasApi.md#getSolarCurrentStats) | **GET** /schemas/{id}/solar/stats/current | 
*SolarSchools.SchemasApi* | [**getSummary**](docs/SchemasApi.md#getSummary) | **GET** /schemas/{id}/summary | 
*SolarSchools.SchemasApi* | [**searchResources**](docs/SchemasApi.md#searchResources) | **GET** /schemas/{id}/resources | 
*SolarSchools.SchemasApi* | [**searchSites**](docs/SchemasApi.md#searchSites) | **GET** /schemas/{id}/sites/search | 
*SolarSchools.SitesApi* | [**callDelete**](docs/SitesApi.md#callDelete) | **DELETE** /sites/{id} | 
*SolarSchools.SitesApi* | [**get**](docs/SitesApi.md#get) | **GET** /sites/{id} | 
*SolarSchools.SitesApi* | [**getEnergyBreakdown**](docs/SitesApi.md#getEnergyBreakdown) | **GET** /sites/{id}/energy/breakdown | 
*SolarSchools.SitesApi* | [**getEnergyDataFeed**](docs/SitesApi.md#getEnergyDataFeed) | **GET** /sites/{id}/energy/data-feed | 
*SolarSchools.SitesApi* | [**getEnergyLifetimeData**](docs/SitesApi.md#getEnergyLifetimeData) | **GET** /sites/{id}/energy/lifetime-data | 
*SolarSchools.SitesApi* | [**getMeters**](docs/SitesApi.md#getMeters) | **GET** /sites/{id}/meters | 
*SolarSchools.SitesApi* | [**getPhysicalMeters**](docs/SitesApi.md#getPhysicalMeters) | **GET** /sites/{id}/meters/physical | 
*SolarSchools.SitesApi* | [**getSummary**](docs/SitesApi.md#getSummary) | **GET** /sites/{id}/summary | 
*SolarSchools.SitesApi* | [**getSupportNotes**](docs/SitesApi.md#getSupportNotes) | **GET** /sites/{id}/support-notes | 
*SolarSchools.SitesApi* | [**getSystems**](docs/SitesApi.md#getSystems) | **GET** /sites/{id}/systems | 
*SolarSchools.SitesApi* | [**getSystemsDataFeed**](docs/SitesApi.md#getSystemsDataFeed) | **GET** /sites/{id}/systems/data-feed | 
*SolarSchools.SitesApi* | [**getVirtualMeters**](docs/SitesApi.md#getVirtualMeters) | **GET** /sites/{id}/meters/virtual | 
*SolarSchools.SitesApi* | [**getWeather**](docs/SitesApi.md#getWeather) | **GET** /sites/{id}/weather | 
*SolarSchools.SitesApi* | [**update**](docs/SitesApi.md#update) | **PUT** /sites/{id} | 
*SolarSchools.VirtualMetersApi* | [**getEnergyBreakdown**](docs/VirtualMetersApi.md#getEnergyBreakdown) | **GET** /virtual-meters/{id}/energy/breakdown | 


## Documentation for Models

 - [SolarSchools.CreateApiKey](docs/CreateApiKey.md)
 - [SolarSchools.CreateContractor](docs/CreateContractor.md)
 - [SolarSchools.CreateDisplayBoard](docs/CreateDisplayBoard.md)
 - [SolarSchools.CreateDisplayBoardDomain](docs/CreateDisplayBoardDomain.md)
 - [SolarSchools.CreateFault](docs/CreateFault.md)
 - [SolarSchools.CreateFaultLog](docs/CreateFaultLog.md)
 - [SolarSchools.CreateFuelLog](docs/CreateFuelLog.md)
 - [SolarSchools.CreateMeter](docs/CreateMeter.md)
 - [SolarSchools.CreateMeterChannel](docs/CreateMeterChannel.md)
 - [SolarSchools.CreateOrganisation](docs/CreateOrganisation.md)
 - [SolarSchools.CreatePopulation](docs/CreatePopulation.md)
 - [SolarSchools.CreateReminder](docs/CreateReminder.md)
 - [SolarSchools.CreateSavingsGuarantee](docs/CreateSavingsGuarantee.md)
 - [SolarSchools.CreateSchema](docs/CreateSchema.md)
 - [SolarSchools.CreateSite](docs/CreateSite.md)
 - [SolarSchools.CreateSitePopulation](docs/CreateSitePopulation.md)
 - [SolarSchools.CreateSolarSystem](docs/CreateSolarSystem.md)
 - [SolarSchools.CreateSupportNote](docs/CreateSupportNote.md)
 - [SolarSchools.CreateTariff](docs/CreateTariff.md)
 - [SolarSchools.CreateTariffCharge](docs/CreateTariffCharge.md)
 - [SolarSchools.CreateTariffChargeMultiplier](docs/CreateTariffChargeMultiplier.md)
 - [SolarSchools.CreateTariffChargeSeason](docs/CreateTariffChargeSeason.md)
 - [SolarSchools.CreateUser](docs/CreateUser.md)
 - [SolarSchools.CreateUserResourceRole](docs/CreateUserResourceRole.md)
 - [SolarSchools.CreateVirtualMeter](docs/CreateVirtualMeter.md)
 - [SolarSchools.DomainHostingActivation](docs/DomainHostingActivation.md)
 - [SolarSchools.ForgotPassword](docs/ForgotPassword.md)
 - [SolarSchools.LogEvent](docs/LogEvent.md)
 - [SolarSchools.Login](docs/Login.md)
 - [SolarSchools.Logout](docs/Logout.md)
 - [SolarSchools.MeterChannel](docs/MeterChannel.md)
 - [SolarSchools.RefreshSession](docs/RefreshSession.md)
 - [SolarSchools.ResetPassword](docs/ResetPassword.md)
 - [SolarSchools.ResourceRole](docs/ResourceRole.md)
 - [SolarSchools.SetChannels](docs/SetChannels.md)
 - [SolarSchools.SetDashboardConfig](docs/SetDashboardConfig.md)
 - [SolarSchools.SetDisplayBoardConfig](docs/SetDisplayBoardConfig.md)
 - [SolarSchools.SetImportTime](docs/SetImportTime.md)
 - [SolarSchools.SetPassword](docs/SetPassword.md)
 - [SolarSchools.SetVirtualMeterChannels](docs/SetVirtualMeterChannels.md)
 - [SolarSchools.TestPermission](docs/TestPermission.md)
 - [SolarSchools.UpdateApiKey](docs/UpdateApiKey.md)
 - [SolarSchools.UpdateContractor](docs/UpdateContractor.md)
 - [SolarSchools.UpdateDisplayBoardDomain](docs/UpdateDisplayBoardDomain.md)
 - [SolarSchools.UpdateFuelLog](docs/UpdateFuelLog.md)
 - [SolarSchools.UpdateMeter](docs/UpdateMeter.md)
 - [SolarSchools.UpdateOrganisation](docs/UpdateOrganisation.md)
 - [SolarSchools.UpdatePopulation](docs/UpdatePopulation.md)
 - [SolarSchools.UpdateReminder](docs/UpdateReminder.md)
 - [SolarSchools.UpdateSavingsGuarantee](docs/UpdateSavingsGuarantee.md)
 - [SolarSchools.UpdateSchema](docs/UpdateSchema.md)
 - [SolarSchools.UpdateSite](docs/UpdateSite.md)
 - [SolarSchools.UpdateSolarSystem](docs/UpdateSolarSystem.md)
 - [SolarSchools.UpdateSupportNote](docs/UpdateSupportNote.md)
 - [SolarSchools.UpdateTariff](docs/UpdateTariff.md)
 - [SolarSchools.UpdateTariffCharge](docs/UpdateTariffCharge.md)
 - [SolarSchools.UpdateTariffChargeMultiplier](docs/UpdateTariffChargeMultiplier.md)
 - [SolarSchools.UpdateTariffChargeSeason](docs/UpdateTariffChargeSeason.md)
 - [SolarSchools.UpdateUser](docs/UpdateUser.md)
 - [SolarSchools.UpdateVirtualMeter](docs/UpdateVirtualMeter.md)
 - [SolarSchools.UpdateWattwatchersDevice](docs/UpdateWattwatchersDevice.md)
 - [SolarSchools.UpdateWattwatchersDeviceChannel](docs/UpdateWattwatchersDeviceChannel.md)
 - [SolarSchools.UserResourceRoles](docs/UserResourceRoles.md)
 - [SolarSchools.VirtualMeterChannel](docs/VirtualMeterChannel.md)


## Documentation for Authorization



### ApiAuthorizer


- **Type**: API key
- **API key parameter name**: Authorization
- **Location**: HTTP header



### ApiSoftAuthorizer


- **Type**: API key
- **API key parameter name**: Authorization
- **Location**: HTTP header

