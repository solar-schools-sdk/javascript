# SolarSchools.UpdateWattwatchersDevice

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**phases** | **Number** |  | 
**channels** | [**[UpdateWattwatchersDeviceChannel]**](UpdateWattwatchersDeviceChannel.md) |  | 


