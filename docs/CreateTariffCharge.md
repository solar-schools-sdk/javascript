# SolarSchools.CreateTariffCharge

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**description** | **String** |  | [optional] 
**type** | **Number** | Type of the charge - Purchase: 1, Feed-In: 2. | 
**group** | **String** | Tag to group similar charges together. | [optional] 
**displayOrder** | **Number** | Order to display within the group. | [optional] 
**chargeTypeUnitId** | **Number** | Unit of the charge type which determines how the pricing is calculated. | 
**unitPrice** | **Number** | Price of a unit. Only used if a season for the period is not provided. | [optional] 
**unitDiscount** | **Number** | Discount amount to apply to the quantity before price calculation. Only used if a season for the period is not provided. | [optional] 
**periodStart** | **String** | Time of the day (HH:mm) when the charge applies for standard times (weekdays). For all-day make the start and end-time the same. Only applicable to energy-based charges. | [optional] 
**periodEnd** | **String** | Time of the day (HH:mm) when the charge finishes for standard times (weekdays). Only applicable to energy-based charges. | [optional] 
**alternatePeriodStart** | **String** | Time of the day (HH:mm) when the charge applies for special days (weekends and potentially public holidays). For all-day make the start and end-time the same. Only applicable to energy-based charges. | [optional] 
**alternatePeriodEnd** | **String** | Time of the day (HH:mm) when the charge finishes for special days (weekends and potentially public holidays). Only applicable to energy-based charges. | [optional] 
**status** | **Number** | Enabled: 1, Disabled: 2. | 
**multipliers** | [**[CreateTariffChargeMultiplier]**](CreateTariffChargeMultiplier.md) |  | [optional] 
**seasons** | [**[CreateTariffChargeSeason]**](CreateTariffChargeSeason.md) | Allows season-specific pricing which overrides the base pricing. | [optional] 


