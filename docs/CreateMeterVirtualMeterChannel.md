# SolarSchools.CreateMeterVirtualMeterChannel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelId** | **Number** | Channel/source ID or number/position on the physical meter. | 
**multiplier** | **Number** |  | [default to 1]


