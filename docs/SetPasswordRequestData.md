# SolarSchools.SetPasswordRequestData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**password** | **String** |  | 
**resource** | **String** |  | [optional] 


