# SolarSchools.CreateUserResourceRole

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**roleId** | **Number** | ID of the role. | 
**resourceType** | **String** | Resource type. | 
**resourceId** | **String** | ID of the resource type. | 


