# SolarSchools.UpdateFuelLog

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**purchaseDate** | **String** | Date of the fuel purchase in the site&#39;s timezone. | 
**fuelTypeId** | **Number** | ID of the fuel type. | 
**quantity** | **Number** | Amount of fuel purchased in litres. | 
**notes** | **String** | Notes applicable to the purchase. | [optional] 


