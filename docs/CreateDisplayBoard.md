# SolarSchools.CreateDisplayBoard

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**resourceType** | **String** | Resource type. | 
**resourceId** | **Number** | ID of the resource type. | 
**key** | **String** | Key of the resource config. | [optional] 
**domainId** | **Number** | ID of the domain the display board will display on. | [optional] 
**config** | **Object** |  | [optional] 


