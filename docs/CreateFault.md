# SolarSchools.CreateFault

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**siteId** | **Number** | ID of the site. | [optional] 
**meterId** | **Number** | ID of the meter which is in fault. Either meterId or solarSystemId must be defined. | [optional] 
**solarSystemId** | **Number** | ID of the solar system which is in fault. Either meterId or solarSystemId must be defined. | [optional] 
**fayltType** | **Number** | Type of the fault. | [optional] 
**severity** | **Number** | Severity of the fault. | [optional] 
**status** | **Number** | Status of the fault. | [optional] 
**description** | **String** |  | [optional] 
**duration** | **Number** | Duration in minutes spent on this log. | [optional] 
**triggerTime** | **String** | Time of the fault in the site&#39;s timezone. | [optional] 


