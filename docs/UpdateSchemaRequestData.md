# SolarSchools.UpdateSchemaRequestData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parentId** | **Number** |  | [optional] 
**name** | **String** |  | 
**type** | **Number** |  | 
**description** | **String** |  | [optional] 
**allowUsers** | **Number** |  | 
**dataAggregationLevel** | **Number** |  | 
**status** | **Number** |  | 


