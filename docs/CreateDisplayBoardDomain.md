# SolarSchools.CreateDisplayBoardDomain

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parentId** | **Number** | Parent domain to inherit from. | [optional] 
**brandName** | **String** | Display brand name of the domain. | 
**appName** | **String** | Display app name of the domain. | 
**domain** | **String** | The domain to host the display board on. | 
**resourceType** | **String** | Resource type to which the domain is available. | [optional] 
**resourceId** | **Number** | ID of the resource type to which the domain is available. | [optional] 
**dnsHostDomain** | **String** | TLD of the DNS hosting zone for internally-hosted domains. | [optional] 
**favicon** | **String** | Location of the favicon to use. | [optional] 
**logo** | **String** | Location of the logo to use. | [optional] 
**status** | **Number** | Status of the domain; Enabled: 1, Disabled: 2. | 


