# SolarSchools.UpdateTariffChargeSeason

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**monthStart** | **Number** | Numerical inclusive start month of the season; 1-12. | 
**monthEnd** | **Number** | Numerical exclusive end month of the season; 1-12. | 
**unitPrice** | **Number** | Price of the tariff in the tariff&#39;s unit for this season. Overrides the base charge value. | 
**unitDiscount** | **Number** | Discount amount to apply to the quantity before price calculation. Overrides the base charge value. | 


