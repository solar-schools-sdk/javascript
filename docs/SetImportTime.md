# SolarSchools.SetImportTime

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**time** | **String** | Date and time to set the import. Defaults to UTC timezone. | [optional] 
**timezone** | **String** | Timezone of the time property. | [optional] 


