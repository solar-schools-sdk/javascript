# SolarSchools.AuthenticationApi

All URIs are relative to *https://api.solarschools.net/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**login**](AuthenticationApi.md#login) | **POST** /authentication/login | 
[**logout**](AuthenticationApi.md#logout) | **POST** /authentication/logout | 
[**refreshSession**](AuthenticationApi.md#refreshSession) | **POST** /authentication/refresh | 
[**testAccess**](AuthenticationApi.md#testAccess) | **GET** /authentication/access-test | 



## login

> Object login(opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.AuthenticationApi();
let opts = {
  'login': new SolarSchools.Login() // Login | Payload for logging in a user.
};
apiInstance.login(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **login** | [**Login**](Login.md)| Payload for logging in a user. | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## logout

> Object logout(opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiAuthorizer
let ApiAuthorizer = defaultClient.authentications['ApiAuthorizer'];
ApiAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.AuthenticationApi();
let opts = {
  'logout': new SolarSchools.Logout() // Logout | Payload for logging out a user.
};
apiInstance.logout(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **logout** | [**Logout**](Logout.md)| Payload for logging out a user. | [optional] 

### Return type

**Object**

### Authorization

[ApiAuthorizer](../README.md#ApiAuthorizer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## refreshSession

> Object refreshSession(opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.AuthenticationApi();
let opts = {
  'attachUser': true, 1, 'true', // String | Attach the user to the response data.
  'refreshSession': new SolarSchools.RefreshSession() // RefreshSession | Payload for refreshing a user session.
};
apiInstance.refreshSession(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **attachUser** | **String**| Attach the user to the response data. | [optional] 
 **refreshSession** | [**RefreshSession**](RefreshSession.md)| Payload for refreshing a user session. | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## testAccess

> Object testAccess()



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiAuthorizer
let ApiAuthorizer = defaultClient.authentications['ApiAuthorizer'];
ApiAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.AuthenticationApi();
apiInstance.testAccess().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[ApiAuthorizer](../README.md#ApiAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

