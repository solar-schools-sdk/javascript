# SolarSchools.CreateMeterChannel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelId** | **String** | Channel/source ID or number/position on the physical meter. | 
**name** | **String** |  | 
**type** | **Number** |  | 
**ctCapacity** | **Number** | Total capacity of the current transformer. | [optional] 
**multiplier** | **Number** | Multiplier for when only 1 phase is monitored on a multi-phase system or data is only partially assigned to this channel. | 
**isReversed** | **Boolean** | Is the CT clamp installed backwards so the data needs to be reversed? | 
**directionContext** | **Boolean** | Indicator for the direction context of the consumption and generation/feed-in - Consumption positive, generation/feed-in negative: false; generation/feed-in positive, consumption negative: true. | [optional] 
**status** | **Number** |  | 


