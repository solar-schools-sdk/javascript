# SolarSchools.UpdateUser

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**role** | **String** |  | 
**firstName** | **String** |  | [optional] 
**surname** | **String** |  | 
**phone** | **String** |  | [optional] 
**street** | **String** |  | [optional] 
**suburb** | **String** |  | [optional] 
**city** | **String** |  | [optional] 
**state** | **String** |  | [optional] 
**postcode** | **String** |  | [optional] 
**country** | **String** |  | [optional] 
**status** | **Number** | Status of the user; Enabled: 1, Disabled: 9. | 


