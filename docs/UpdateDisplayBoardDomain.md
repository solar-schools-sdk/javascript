# SolarSchools.UpdateDisplayBoardDomain

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parentId** | **Number** | Parent domain to inherit from. | [optional] 
**brandName** | **String** | Display brand name of the domain. | 
**appName** | **String** | Display app name of the domain. | 
**apiUri** | **String** | URI of the API to use. Only applicable to root-level domains. If a parent ID is supplied, this property is ignored. | [optional] 
**cdn** | **String** | URI of the CDN to use. Only applicable to root-level domains. If a parent ID is supplied, this property is ignored. | [optional] 
**dnsHostDomain** | **String** | TLD of the DNS hosting zone for internally-hosted domains. | [optional] 
**favicon** | **String** | Location of the favicon to use. | [optional] 
**logo** | **String** | Location of the logo to use. | [optional] 
**status** | **Number** | Status of the domain; Enabled: 1, Disabled: 2. | [optional] 


