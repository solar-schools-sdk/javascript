# SolarSchools.SetVirtualMeterChannels

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channels** | [**[VirtualMeterChannel]**](VirtualMeterChannel.md) |  | [optional] 


