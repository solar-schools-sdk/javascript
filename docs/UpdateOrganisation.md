# SolarSchools.UpdateOrganisation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**schemaId** | **Number** |  | 
**name** | **String** |  | 
**slug** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**status** | **Boolean** |  | 


