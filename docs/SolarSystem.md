# SolarSchools.SolarSystem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**installerId** | **Number** | ID of the contractor who installed the system. | 
**maintainerId** | **Number** | ID of the contractor who is maintaining the system. | [optional] 
**installationDate** | **String** | Date of when the solar system was installed. | [optional] 
**location** | **String** |  | [optional] 
**roofDirection** | **String** |  | [optional] 
**roofPitch** | **String** |  | [optional] 
**panelPitch** | **String** |  | [optional] 
**inverter** | **String** |  | [optional] 
**inverterUrl** | **String** |  | [optional] 
**inverterCapacity** | **String** | Maximum capacity of the inverter in Watts. | [optional] 
**panelType** | **String** |  | [optional] 
**panelSize** | **String** |  | [optional] 
**panelQuantity** | **Number** |  | [optional] 
**panelCapacity** | **Number** | Maximum capacity of the inverter in Watts. | [optional] 
**peakCapacity** | **Number** | System peak capacity in Wp. | [optional] 
**minimumCapacity** | **Number** | Minimum capacity required from the system in Watts. | [optional] 
**notes** | **String** |  | [optional] 
**status** | **Number** |  | [optional] 


