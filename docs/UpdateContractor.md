# SolarSchools.UpdateContractor

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**schemaId** | **Number** |  | [optional] 
**name** | **String** |  | 
**phone** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**street** | **String** |  | [optional] 
**suburb** | **String** |  | [optional] 
**city** | **String** |  | [optional] 
**postcode** | **String** |  | [optional] 
**countryCode** | **String** |  | 
**countryDivisionId** | **Number** |  | [optional] 
**status** | **Number** | Status of the contractor; Enabled: 1, Disabled: 9. | 


