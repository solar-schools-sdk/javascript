# SolarSchools.CreateTariff

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**virtualMeterId** | **Number** | ID of the grid virtual meter that the tariff will apply to. | 
**name** | **String** |  | 
**energyProvider** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**startDate** | **String** | The start date for when this tariff scheme is in effect for the meter. | 
**endDate** | **String** | The end date when this tariff scheme ends. If null it indicates the scheme is ongoing. | [optional] 
**billingPeriodDuration** | **Number** | Billing period duration in months. Valid values: 1, 2, 3, 4, 6, 12. | 
**demandPeriodDuration** | **Number** | Duration of the demand periods in minutes. Valid values: 5, 10, 15, 30, 60, 120, 180, 240, 360, 720, 1440; default: 30. | [optional] 
**priceUnit** | **Number** | Currency unit; $: 1. | [optional] 
**gstRate** | **Number** | GST value to be applied to the charge total. Null indicates there is no GST or GST is already applied. | [optional] 
**status** | **Number** |  | 
**charges** | [**[CreateTariffCharge]**](CreateTariffCharge.md) |  | [optional] 


