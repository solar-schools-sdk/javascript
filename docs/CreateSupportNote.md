# SolarSchools.CreateSupportNote

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**siteId** | **Number** |  | 
**userId** | **Number** |  | 
**resourceId** | **Number** |  | [optional] 
**referenceUserId** | **Number** |  | [optional] 
**type** | **Number** |  | 
**duration** | **String** | Time spent on the event. Must be in format: &#39;hh:mm&#39;. | [optional] 
**note** | **String** |  | 


