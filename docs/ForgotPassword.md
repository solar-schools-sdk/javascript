# SolarSchools.ForgotPassword

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  | 
**resource** | **String** |  | [optional] 


