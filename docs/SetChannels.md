# SolarSchools.SetChannels

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channels** | [**[MeterChannel]**](MeterChannel.md) |  | [optional] 


