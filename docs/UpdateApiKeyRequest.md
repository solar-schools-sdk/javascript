# SolarSchools.UpdateApiKeyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**status** | **Number** |  | 
**domains** | **[String]** |  | 


