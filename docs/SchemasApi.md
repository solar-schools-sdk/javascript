# SolarSchools.SchemasApi

All URIs are relative to *https://api.solarschools.net/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get**](SchemasApi.md#get) | **GET** /schemas/{id} | 
[**getContractors**](SchemasApi.md#getContractors) | **GET** /schemas/{id}/contractors | 
[**getEnergyBreakdown**](SchemasApi.md#getEnergyBreakdown) | **GET** /schemas/{id}/energy-breakdown | 
[**getEnergyDataFeed**](SchemasApi.md#getEnergyDataFeed) | **GET** /schemas/{id}/energy/data-feed | 
[**getEnergyLifetimeSummary**](SchemasApi.md#getEnergyLifetimeSummary) | **GET** /schemas/{id}/energy/summary/lifetime | 
[**getList**](SchemasApi.md#getList) | **GET** /schemas/ | 
[**getMapSites**](SchemasApi.md#getMapSites) | **GET** /schemas/{id}/sites/map | 
[**getResourceBySlug**](SchemasApi.md#getResourceBySlug) | **GET** /schemas/{id}/resources/{slug} | 
[**getSites**](SchemasApi.md#getSites) | **GET** /schemas/{id}/sites | 
[**getSolarCurrentStats**](SchemasApi.md#getSolarCurrentStats) | **GET** /schemas/{id}/solar/stats/current | 
[**getSummary**](SchemasApi.md#getSummary) | **GET** /schemas/{id}/summary | 
[**searchResources**](SchemasApi.md#searchResources) | **GET** /schemas/{id}/resources | 
[**searchSites**](SchemasApi.md#searchSites) | **GET** /schemas/{id}/sites/search | 



## get

> Object get(id)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiAuthorizer
let ApiAuthorizer = defaultClient.authentications['ApiAuthorizer'];
ApiAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.SchemasApi();
let id = 56; // Number | 
apiInstance.get(id).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

**Object**

### Authorization

[ApiAuthorizer](../README.md#ApiAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getContractors

> Object getContractors(id, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiAuthorizer
let ApiAuthorizer = defaultClient.authentications['ApiAuthorizer'];
ApiAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.SchemasApi();
let id = 56; // Number | 
let opts = {
  'page': 56, // Number | Page of results to return.
  'limit': 56, // Number | Limit of results to return.
  'status': 56, // Number | Filter results by status.
  'name': "name_example", // String | Filter results by name.
  'email': "email_example", // String | Filter results by email.
  'state': "state_example", // String | Filter results by state.
  'country': "country_example" // String | Filter results by country.
};
apiInstance.getContractors(id, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **page** | **Number**| Page of results to return. | [optional] 
 **limit** | **Number**| Limit of results to return. | [optional] 
 **status** | **Number**| Filter results by status. | [optional] 
 **name** | **String**| Filter results by name. | [optional] 
 **email** | **String**| Filter results by email. | [optional] 
 **state** | **String**| Filter results by state. | [optional] 
 **country** | **String**| Filter results by country. | [optional] 

### Return type

**Object**

### Authorization

[ApiAuthorizer](../README.md#ApiAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getEnergyBreakdown

> Object getEnergyBreakdown(id)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiAuthorizer
let ApiAuthorizer = defaultClient.authentications['ApiAuthorizer'];
ApiAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.SchemasApi();
let id = 56; // Number | 
apiInstance.getEnergyBreakdown(id).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

**Object**

### Authorization

[ApiAuthorizer](../README.md#ApiAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getEnergyDataFeed

> Object getEnergyDataFeed(id, start, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.SchemasApi();
let id = 56; // Number | 
let start = 2018-12-03 or 2018-12-03 13:00:00 or 2018-12-03T13:00:00; // String | The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone.
let opts = {
  'end': 2018-12-03 or 2018-12-03 13:00:00 or 2018-12-03T13:00:00, // String | The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - format without a timezone.
  'aggregation': month or 30 minute // String | Suggested aggregation of the data. Is not guaranteed if date range is too large for the aggregation type.
};
apiInstance.getEnergyDataFeed(id, start, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **start** | **String**| The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone. | 
 **end** | **String**| The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - format without a timezone. | [optional] 
 **aggregation** | **String**| Suggested aggregation of the data. Is not guaranteed if date range is too large for the aggregation type. | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getEnergyLifetimeSummary

> Object getEnergyLifetimeSummary(id, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.SchemasApi();
let id = 56; // Number | 
let opts = {
  'timezone': Australia/Brisbane // String | Users IANA timezone.
};
apiInstance.getEnergyLifetimeSummary(id, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **timezone** | **String**| Users IANA timezone. | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getList

> Object getList(opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiAuthorizer
let ApiAuthorizer = defaultClient.authentications['ApiAuthorizer'];
ApiAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.SchemasApi();
let opts = {
  'type': 56 // Number | Type filter.
};
apiInstance.getList(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **Number**| Type filter. | [optional] 

### Return type

**Object**

### Authorization

[ApiAuthorizer](../README.md#ApiAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMapSites

> Object getMapSites(id, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.SchemasApi();
let id = 56; // Number | 
let opts = {
  'filter': "filter_example" // String | Filter for the name of the site.
};
apiInstance.getMapSites(id, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **filter** | **String**| Filter for the name of the site. | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getResourceBySlug

> Object getResourceBySlug(id, slug, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.SchemasApi();
let id = 56; // Number | 
let slug = "slug_example"; // String | Slug of the resource.
let opts = {
  'timezone': "timezone_example", // String | IANA timezone of the caller to determine the contextual current day. Defaults to Australia/Brisbane; e.g.: Australia/Brisbane
  'weather': true, // Boolean | Attach weather if it is a site resource.
  'summary': true // Boolean | Attach summary if it is a site resource.
};
apiInstance.getResourceBySlug(id, slug, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **slug** | **String**| Slug of the resource. | 
 **timezone** | **String**| IANA timezone of the caller to determine the contextual current day. Defaults to Australia/Brisbane; e.g.: Australia/Brisbane | [optional] 
 **weather** | **Boolean**| Attach weather if it is a site resource. | [optional] 
 **summary** | **Boolean**| Attach summary if it is a site resource. | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSites

> Object getSites(id, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.SchemasApi();
let id = 56; // Number | 
let opts = {
  'page': 56, // Number | Page of results to return.
  'limit': 56, // Number | Limit of results to return.
  'filter': "filter_example" // String | Filter for the organisation name of the site.
};
apiInstance.getSites(id, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **page** | **Number**| Page of results to return. | [optional] 
 **limit** | **Number**| Limit of results to return. | [optional] 
 **filter** | **String**| Filter for the organisation name of the site. | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSolarCurrentStats

> Object getSolarCurrentStats(id)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.SchemasApi();
let id = 56; // Number | 
apiInstance.getSolarCurrentStats(id).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSummary

> Object getSummary(id, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiSoftAuthorizer
let ApiSoftAuthorizer = defaultClient.authentications['ApiSoftAuthorizer'];
ApiSoftAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiSoftAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.SchemasApi();
let id = 56; // Number | 
let opts = {
  'timezone': Australia/Brisbane // String | Users IANA timezone.
};
apiInstance.getSummary(id, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **timezone** | **String**| Users IANA timezone. | [optional] 

### Return type

**Object**

### Authorization

[ApiSoftAuthorizer](../README.md#ApiSoftAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## searchResources

> Object searchResources(id, term, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.SchemasApi();
let id = 56; // Number | 
let term = brisbane; // String | The search term.
let opts = {
  'page': 56, // Number | Page of results to return.
  'limit': 56, // Number | Limit of results to return.
  'timezone': Australia/Brisbane, // String | Users IANA timezone.
  'extendedDetails': true, // Boolean | Attach extended details to the sites.
  'stats': true, // Boolean | Attach stats to the sites.
  'weather': true, // Boolean | Attach weather to the sites.
  'resourceMode': "resourceMode_example" // String | Search mode for returning different types of resources; all (all organisations and sites), organisations (single-sibling sites and organisations with 0 or more than 1 site) or sites (sites only).
};
apiInstance.searchResources(id, term, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **term** | **String**| The search term. | 
 **page** | **Number**| Page of results to return. | [optional] 
 **limit** | **Number**| Limit of results to return. | [optional] 
 **timezone** | **String**| Users IANA timezone. | [optional] 
 **extendedDetails** | **Boolean**| Attach extended details to the sites. | [optional] 
 **stats** | **Boolean**| Attach stats to the sites. | [optional] 
 **weather** | **Boolean**| Attach weather to the sites. | [optional] 
 **resourceMode** | **String**| Search mode for returning different types of resources; all (all organisations and sites), organisations (single-sibling sites and organisations with 0 or more than 1 site) or sites (sites only). | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## searchSites

> Object searchSites(id, term, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.SchemasApi();
let id = 56; // Number | 
let term = brisbane; // String | The search term.
let opts = {
  'page': 56, // Number | Page of results to return.
  'limit': 56, // Number | Limit of results to return.
  'timezone': Australia/Brisbane // String | Users IANA timezone.
};
apiInstance.searchSites(id, term, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **term** | **String**| The search term. | 
 **page** | **Number**| Page of results to return. | [optional] 
 **limit** | **Number**| Limit of results to return. | [optional] 
 **timezone** | **String**| Users IANA timezone. | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

