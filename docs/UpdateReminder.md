# SolarSchools.UpdateReminder

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **Number** |  | 
**contactId** | **Number** |  | [optional] 
**siteId** | **Number** |  | [optional] 
**faultId** | **Number** |  | [optional] 
**message** | **String** |  | 
**status** | **Number** |  | 
**reminderTime** | **String** | Time of the reminder in UTC. | 


