# SolarSchools.CreateMessage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**siteId** | **Number** |  | 
**userId** | **Number** |  | [optional] 
**resourceType** | **Number** |  | [optional] 
**resourceId** | **Number** |  | [optional] 
**referenceUserId** | **Number** |  | [optional] 
**type** | **Number** |  | 
**eventDate** | **String** | Date of the event. Must be in ISO format; e.g.: 2020-01-13. | 
**duration** | **String** | Time spent on the event. Must be in format: &#39;hh:mm&#39;. | [optional] 
**message** | **String** |  | 


