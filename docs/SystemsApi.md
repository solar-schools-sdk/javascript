# SolarSchools.SystemsApi

All URIs are relative to *https://api.solarschools.net/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDataFeed**](SystemsApi.md#getDataFeed) | **GET** /systems/{id}/data-feed | 
[**getLifetimeData**](SystemsApi.md#getLifetimeData) | **GET** /systems/{id}/lifetime-data | 



## getDataFeed

> Object getDataFeed(id, start, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.SystemsApi();
let id = 56; // Number | 
let start = 2018-12-03 or 2018-12-03 13:00:00; // String | The date and time in YYYY-MM-DD or YYYY-MM-DD HH:MM:SS format.
let opts = {
  'end': 2018-12-03 or 2018-12-03 13:00:00 // String | The date and time in YYYY-MM-DD or YYYY-MM-DD HH:MM:SS format.
};
apiInstance.getDataFeed(id, start, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **start** | **String**| The date and time in YYYY-MM-DD or YYYY-MM-DD HH:MM:SS format. | 
 **end** | **String**| The date and time in YYYY-MM-DD or YYYY-MM-DD HH:MM:SS format. | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getLifetimeData

> Object getLifetimeData(id)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiAuthorizer
let ApiAuthorizer = defaultClient.authentications['ApiAuthorizer'];
ApiAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiAuthorizer.apiKeyPrefix = 'Token';
// Configure API key authorization: ApiKey
let ApiKey = defaultClient.authentications['ApiKey'];
ApiKey.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiKey.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.SystemsApi();
let id = 56; // Number | 
apiInstance.getLifetimeData(id).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

**Object**

### Authorization

[ApiAuthorizer](../README.md#ApiAuthorizer), [ApiKey](../README.md#ApiKey)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

