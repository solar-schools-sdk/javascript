# SolarSchools.CreateVirtualMeterChannel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelId** | **Number** |  | 
**multiplier** | **Number** |  | 


