# SolarSchools.CreateTariffChargeMultiplier

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**multiplierId** | **Number** | ID of the multiplier to apply to the charge. | 
**multiplier** | **Number** | Multiplier to apply to the charge in decimal format. | 


