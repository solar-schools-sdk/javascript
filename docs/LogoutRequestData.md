# SolarSchools.LogoutRequestData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**refreshToken** | **String** |  | [optional] 
**resource** | **String** |  | [optional] 


