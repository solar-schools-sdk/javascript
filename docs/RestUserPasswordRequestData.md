# SolarSchools.RestUserPasswordRequestData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  | 
**resetCode** | **String** |  | 
**newPassword** | **String** |  | 


