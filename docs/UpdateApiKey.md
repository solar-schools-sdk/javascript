# SolarSchools.UpdateApiKey

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**status** | **Number** |  | 
**domains** | **[String]** |  | 


