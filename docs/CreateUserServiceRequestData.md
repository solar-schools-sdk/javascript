# SolarSchools.CreateUserServiceRequestData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**role** | **String** |  | 
**firstName** | **String** |  | [optional] 
**surname** | **String** |  | 
**email** | **String** |  | 
**password** | **String** |  | 
**phone** | **String** |  | [optional] 
**street** | **String** |  | [optional] 
**suburb** | **String** |  | [optional] 
**city** | **String** |  | [optional] 
**state** | **String** |  | [optional] 
**postcode** | **String** |  | [optional] 
**country** | **String** |  | [optional] 
**resourceRoles** | [**[CreateUserServiceResourceRole]**](CreateUserServiceResourceRole.md) |  | [optional] 


