# SolarSchools.VirtualMetersApi

All URIs are relative to *https://api.solarschools.net/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getEnergyBreakdown**](VirtualMetersApi.md#getEnergyBreakdown) | **GET** /virtual-meters/{id}/energy/breakdown | 



## getEnergyBreakdown

> Object getEnergyBreakdown(id, start, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.VirtualMetersApi();
let id = 56; // Number | 
let start = 2018-12-03 or 2018-12-03 13:00:00 or 2018-12-03T13:00:00; // String | The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone.
let opts = {
  'end': 2018-12-03 or 2018-12-03 13:00:00 or 2018-12-03T13:00:00, // String | The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone.
  'aggregation': month or 30 minute, // String | Suggested aggregation of the data. Is not guaranteed if date range is too large for the aggregation type.
  'grouping': "grouping_example" // String | Grouping of the submeters breakdown; type, group or none; default: none.
};
apiInstance.getEnergyBreakdown(id, start, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **start** | **String**| The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone. | 
 **end** | **String**| The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone. | [optional] 
 **aggregation** | **String**| Suggested aggregation of the data. Is not guaranteed if date range is too large for the aggregation type. | [optional] 
 **grouping** | **String**| Grouping of the submeters breakdown; type, group or none; default: none. | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

