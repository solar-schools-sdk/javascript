# SolarSchools.UpdateSupportNote

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **Number** |  | 
**type** | **Number** |  | 
**duration** | **String** | Time spent on the event. Must be in format: &#39;hh:mm&#39;. | [optional] 
**note** | **String** |  | 


