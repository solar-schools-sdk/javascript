# SolarSchools.ForgotPasswordRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  | 
**resource** | **String** |  | [optional] 


