# SolarSchools.SetDisplayBoardConfig

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**domainId** | **Number** | ID of the domain the display board will display on. | 
**config** | **Object** |  | 


