# SolarSchools.DataApi

All URIs are relative to *https://api.solarschools.net/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getChildEnergyBreakdown**](DataApi.md#getChildEnergyBreakdown) | **GET** /data/{resourceType}/{resourceId}/energy/child-breakdown | 
[**getChildEnergyStats**](DataApi.md#getChildEnergyStats) | **GET** /data/{resourceType}/{resourceId}/energy/child-stats | 
[**getChildSolarStats**](DataApi.md#getChildSolarStats) | **GET** /data/{resourceType}/{resourceId}/solar/child-stats | 
[**getComparison**](DataApi.md#getComparison) | **GET** /data/{resourceType}/{resourceId}/compare/{property} | 
[**getEmissionsFeed**](DataApi.md#getEmissionsFeed) | **GET** /data/{resourceType}/{resourceId}/emissions/feed | 
[**getEnergyFeed**](DataApi.md#getEnergyFeed) | **GET** /data/{resourceType}/{resourceId}/energy/feed | 
[**getEnergyPredictions**](DataApi.md#getEnergyPredictions) | **GET** /data/{resourceType}/{resourceId}/energy/predictions | 
[**getEnergyStats**](DataApi.md#getEnergyStats) | **GET** /data/{resourceType}/{resourceId}/energy/stats | 
[**getSiteEventEnergyFeed**](DataApi.md#getSiteEventEnergyFeed) | **GET** /data/sites/{siteId}/events/{eventId}/energy/feed | 
[**getSitesEnergyRanking**](DataApi.md#getSitesEnergyRanking) | **GET** /data/{resourceType}/{resourceId}/sites/energy/ranking | 
[**getSummary**](DataApi.md#getSummary) | **GET** /data/{resourceType}/{resourceId}/summary | 
[**getWaterFeed**](DataApi.md#getWaterFeed) | **GET** /data/{resourceType}/{resourceId}/water/feed | 
[**getWaterStats**](DataApi.md#getWaterStats) | **GET** /data/{resourceType}/{resourceId}/water/stats | 



## getChildEnergyBreakdown

> Object getChildEnergyBreakdown(resourceType, resourceId, type, start, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiSoftAuthorizer
let ApiSoftAuthorizer = defaultClient.authentications['ApiSoftAuthorizer'];
ApiSoftAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiSoftAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.DataApi();
let resourceType = "resourceType_example"; // String | Type of the resource.
let resourceId = 56; // Number | ID of the resource.
let type = grid or solar; // String | The type of data to return.
let start = 2018-12-03 or 2018-12-03 13:00:00 or 2018-12-03T13:00:00; // String | The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone.
let opts = {
  'period': year, month, day, previous day or previous 28 days., // String | Pre-defined period of time to retrieve.
  'end': 2018-12-03 or 2018-12-03 13:00:00 or 2018-12-03T13:00:00 // String | The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - format without a timezone.
};
apiInstance.getChildEnergyBreakdown(resourceType, resourceId, type, start, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resourceType** | **String**| Type of the resource. | 
 **resourceId** | **Number**| ID of the resource. | 
 **type** | **String**| The type of data to return. | 
 **start** | **String**| The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone. | 
 **period** | **String**| Pre-defined period of time to retrieve. | [optional] 
 **end** | **String**| The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - format without a timezone. | [optional] 

### Return type

**Object**

### Authorization

[ApiSoftAuthorizer](../README.md#ApiSoftAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getChildEnergyStats

> Object getChildEnergyStats(resourceType, resourceId)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiSoftAuthorizer
let ApiSoftAuthorizer = defaultClient.authentications['ApiSoftAuthorizer'];
ApiSoftAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiSoftAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.DataApi();
let resourceType = "resourceType_example"; // String | Type of the resource.
let resourceId = 56; // Number | ID of the resource.
apiInstance.getChildEnergyStats(resourceType, resourceId).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resourceType** | **String**| Type of the resource. | 
 **resourceId** | **Number**| ID of the resource. | 

### Return type

**Object**

### Authorization

[ApiSoftAuthorizer](../README.md#ApiSoftAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getChildSolarStats

> Object getChildSolarStats(resourceType, resourceId)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.DataApi();
let resourceType = "resourceType_example"; // String | Type of the resource.
let resourceId = 56; // Number | ID of the resource.
apiInstance.getChildSolarStats(resourceType, resourceId).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resourceType** | **String**| Type of the resource. | 
 **resourceId** | **Number**| ID of the resource. | 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getComparison

> Object getComparison(resourceType, resourceId, property, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.DataApi();
let resourceType = "resourceType_example"; // String | Type of the resource; schemas, organisations, sites, gridtree or grid-tree.
let resourceId = 56; // Number | ID of the resource.
let property = "property_example"; // String | Property to compare; co2e.
let opts = {
  'attendanceProfile': "attendanceProfile_example", // String | The profile for determining attendance based on opening hours at the site; standard, extended, exceptional, extreme, school. Default: standard.
  'attendancePunchdown': null, // Number | The attendance difference to allow comparisons. Restricts high-attendance days from comparing to low-attendance days such as weekends or public holidays (as defined by the attendance profile); 0-1. Default: 0.3.
  'startOfWeek': "startOfWeek_example" // String | The day of the start the week for the week comparisons; Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday. Default: Sunday
};
apiInstance.getComparison(resourceType, resourceId, property, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resourceType** | **String**| Type of the resource; schemas, organisations, sites, gridtree or grid-tree. | 
 **resourceId** | **Number**| ID of the resource. | 
 **property** | **String**| Property to compare; co2e. | 
 **attendanceProfile** | **String**| The profile for determining attendance based on opening hours at the site; standard, extended, exceptional, extreme, school. Default: standard. | [optional] 
 **attendancePunchdown** | [**Number**](.md)| The attendance difference to allow comparisons. Restricts high-attendance days from comparing to low-attendance days such as weekends or public holidays (as defined by the attendance profile); 0-1. Default: 0.3. | [optional] 
 **startOfWeek** | **String**| The day of the start the week for the week comparisons; Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday. Default: Sunday | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getEmissionsFeed

> Object getEmissionsFeed(resourceType, resourceId, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiAuthorizer
let ApiAuthorizer = defaultClient.authentications['ApiAuthorizer'];
ApiAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.DataApi();
let resourceType = "resourceType_example"; // String | Type of the resource.
let resourceId = 56; // Number | ID of the resource.
let opts = {
  'start': 2018-12-03, // String | The date in ISO format - YYYY-MM-DD - without a timezone.
  'end': 2018-12-03, // String | The date in ISO format - YYYY-MM-DD - format without a timezone.
  'aggregation': "aggregation_example", // String | Suggested aggregation of the data. Is not guaranteed if date range is too large for the aggregation type. Day, month or year.
  'includeDisabled': true // Boolean | Include disabled virtual meter data sources.
};
apiInstance.getEmissionsFeed(resourceType, resourceId, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resourceType** | **String**| Type of the resource. | 
 **resourceId** | **Number**| ID of the resource. | 
 **start** | **String**| The date in ISO format - YYYY-MM-DD - without a timezone. | [optional] 
 **end** | **String**| The date in ISO format - YYYY-MM-DD - format without a timezone. | [optional] 
 **aggregation** | **String**| Suggested aggregation of the data. Is not guaranteed if date range is too large for the aggregation type. Day, month or year. | [optional] 
 **includeDisabled** | **Boolean**| Include disabled virtual meter data sources. | [optional] 

### Return type

**Object**

### Authorization

[ApiAuthorizer](../README.md#ApiAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getEnergyFeed

> Object getEnergyFeed(resourceType, resourceId, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiSoftAuthorizer
let ApiSoftAuthorizer = defaultClient.authentications['ApiSoftAuthorizer'];
ApiSoftAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiSoftAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.DataApi();
let resourceType = "resourceType_example"; // String | Type of the resource.
let resourceId = 56; // Number | ID of the resource.
let opts = {
  'start': 2018-12-03 or 2018-12-03 13:00:00 or 2018-12-03T13:00:00, // String | The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone.
  'end': 2018-12-03 or 2018-12-03 13:00:00 or 2018-12-03T13:00:00, // String | The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - format without a timezone.
  'aggregation': "aggregation_example", // String | Suggested aggregation of the data. Is not guaranteed if date range is too large for the aggregation type. 5 minute, 30 minute, day, month or year.
  'weather': true, // Boolean | Attach weather to the data.
  'psh': true, // Boolean | Attach sunlight hours to the data.
  'emissionsFactors': true, // Boolean | Attach emissions factors to feed. Only available on site and grid-meter resource types and 5 minute aggregations.
  'includeDisabled': true // Boolean | Include disabled virtual meter data sources.
};
apiInstance.getEnergyFeed(resourceType, resourceId, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resourceType** | **String**| Type of the resource. | 
 **resourceId** | **Number**| ID of the resource. | 
 **start** | **String**| The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone. | [optional] 
 **end** | **String**| The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - format without a timezone. | [optional] 
 **aggregation** | **String**| Suggested aggregation of the data. Is not guaranteed if date range is too large for the aggregation type. 5 minute, 30 minute, day, month or year. | [optional] 
 **weather** | **Boolean**| Attach weather to the data. | [optional] 
 **psh** | **Boolean**| Attach sunlight hours to the data. | [optional] 
 **emissionsFactors** | **Boolean**| Attach emissions factors to feed. Only available on site and grid-meter resource types and 5 minute aggregations. | [optional] 
 **includeDisabled** | **Boolean**| Include disabled virtual meter data sources. | [optional] 

### Return type

**Object**

### Authorization

[ApiSoftAuthorizer](../README.md#ApiSoftAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getEnergyPredictions

> Object getEnergyPredictions(resourceType, resourceId, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiAuthorizer
let ApiAuthorizer = defaultClient.authentications['ApiAuthorizer'];
ApiAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.DataApi();
let resourceType = "resourceType_example"; // String | Type of the resource. Must be sites, gridtree or grid-tree.
let resourceId = 56; // Number | ID of the site or grid-virtual meter.
let opts = {
  'start': 2018-12-03, // String | The local date for the start of the historical data in ISO format - YYYY-MM-DD. Must be in the past - defaults to start of the most recent complete data block with maximum of 3 years.
  'startPrediction': 2018-12-03, // String | The local date for the start of the predictions data in ISO format - YYYY-MM-DD. Must be higher than the start date and in the past.
  'end': 2018-12-03, // String | The local date of the maximum future prediction in ISO format - YYYY-MM-DD. Maximum 1 year in the future
  'includeDisabled': true // Boolean | Include disabled virtual meter data sources.
};
apiInstance.getEnergyPredictions(resourceType, resourceId, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resourceType** | **String**| Type of the resource. Must be sites, gridtree or grid-tree. | 
 **resourceId** | **Number**| ID of the site or grid-virtual meter. | 
 **start** | **String**| The local date for the start of the historical data in ISO format - YYYY-MM-DD. Must be in the past - defaults to start of the most recent complete data block with maximum of 3 years. | [optional] 
 **startPrediction** | **String**| The local date for the start of the predictions data in ISO format - YYYY-MM-DD. Must be higher than the start date and in the past. | [optional] 
 **end** | **String**| The local date of the maximum future prediction in ISO format - YYYY-MM-DD. Maximum 1 year in the future | [optional] 
 **includeDisabled** | **Boolean**| Include disabled virtual meter data sources. | [optional] 

### Return type

**Object**

### Authorization

[ApiAuthorizer](../README.md#ApiAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getEnergyStats

> Object getEnergyStats(resourceType, resourceId, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiSoftAuthorizer
let ApiSoftAuthorizer = defaultClient.authentications['ApiSoftAuthorizer'];
ApiSoftAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiSoftAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.DataApi();
let resourceType = "resourceType_example"; // String | Type of the resource.
let resourceId = 56; // Number | ID of the resource.
let opts = {
  'start': 2018-12-03, // String | The date in ISO format - YYYY-MM-DD - without a timezone. If no date is provided, the data will default to lifetime.
  'end': 2018-12-03 // String | The date in ISO format - YYYY-MM-DD - without a timezone.
};
apiInstance.getEnergyStats(resourceType, resourceId, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resourceType** | **String**| Type of the resource. | 
 **resourceId** | **Number**| ID of the resource. | 
 **start** | **String**| The date in ISO format - YYYY-MM-DD - without a timezone. If no date is provided, the data will default to lifetime. | [optional] 
 **end** | **String**| The date in ISO format - YYYY-MM-DD - without a timezone. | [optional] 

### Return type

**Object**

### Authorization

[ApiSoftAuthorizer](../README.md#ApiSoftAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSiteEventEnergyFeed

> Object getSiteEventEnergyFeed(siteId, eventId, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiSoftAuthorizer
let ApiSoftAuthorizer = defaultClient.authentications['ApiSoftAuthorizer'];
ApiSoftAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiSoftAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.DataApi();
let siteId = 56; // Number | ID of the site.
let eventId = "eventId_example"; // String | ID of the event. Number or \"latest\".
let opts = {
  'start': 2018-12-03 or 2018-12-03 13:00:00 or 2018-12-03T13:00:00, // String | The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone.
  'end': 2018-12-03 or 2018-12-03 13:00:00 or 2018-12-03T13:00:00, // String | The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - format without a timezone.
  'aggregation': "aggregation_example", // String | Suggested aggregation of the data. Is not guaranteed if date range is too large for the aggregation type. 5 minute, 30 minute, day, month or year.
  'weather': true, // Boolean | Attach weather to the data.
  'includeDisabled': true // Boolean | Include disabled virtual meter data sources.
};
apiInstance.getSiteEventEnergyFeed(siteId, eventId, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **siteId** | **Number**| ID of the site. | 
 **eventId** | **String**| ID of the event. Number or \&quot;latest\&quot;. | 
 **start** | **String**| The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone. | [optional] 
 **end** | **String**| The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - format without a timezone. | [optional] 
 **aggregation** | **String**| Suggested aggregation of the data. Is not guaranteed if date range is too large for the aggregation type. 5 minute, 30 minute, day, month or year. | [optional] 
 **weather** | **Boolean**| Attach weather to the data. | [optional] 
 **includeDisabled** | **Boolean**| Include disabled virtual meter data sources. | [optional] 

### Return type

**Object**

### Authorization

[ApiSoftAuthorizer](../README.md#ApiSoftAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSitesEnergyRanking

> Object getSitesEnergyRanking(resourceType, resourceId, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.DataApi();
let resourceType = "resourceType_example"; // String | Type of the resource.
let resourceId = 56; // Number | ID of the resource.
let opts = {
  'order': asc or desc, // String | Ordering of the ranking.
  'averageByPopulation': true // Boolean | Should the consumption be averaged by the site's population?
};
apiInstance.getSitesEnergyRanking(resourceType, resourceId, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resourceType** | **String**| Type of the resource. | 
 **resourceId** | **Number**| ID of the resource. | 
 **order** | **String**| Ordering of the ranking. | [optional] 
 **averageByPopulation** | **Boolean**| Should the consumption be averaged by the site&#39;s population? | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSummary

> Object getSummary(resourceType, resourceId, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiSoftAuthorizer
let ApiSoftAuthorizer = defaultClient.authentications['ApiSoftAuthorizer'];
ApiSoftAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiSoftAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.DataApi();
let resourceType = "resourceType_example"; // String | Type of the resource.
let resourceId = 56; // Number | ID of the resource.
let opts = {
  'timezone': Australia/Brisbane // String | The end-users IANA timezone.
};
apiInstance.getSummary(resourceType, resourceId, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resourceType** | **String**| Type of the resource. | 
 **resourceId** | **Number**| ID of the resource. | 
 **timezone** | **String**| The end-users IANA timezone. | [optional] 

### Return type

**Object**

### Authorization

[ApiSoftAuthorizer](../README.md#ApiSoftAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getWaterFeed

> Object getWaterFeed(resourceType, resourceId, start, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.DataApi();
let resourceType = "resourceType_example"; // String | Type of the resource; schemas, organisations, sites or meters.
let resourceId = 56; // Number | ID of the resource.
let start = 2018-12-03 or 2018-12-03 13:00:00 or 2018-12-03T13:00:00; // String | The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone.
let opts = {
  'end': 2018-12-03 or 2018-12-03 13:00:00 or 2018-12-03T13:00:00, // String | The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - format without a timezone.
  'aggregation': "aggregation_example" // String | Suggested aggregation of the data. Is not guaranteed if date range is too large for the aggregation type. 15 minute, day, month or year.
};
apiInstance.getWaterFeed(resourceType, resourceId, start, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resourceType** | **String**| Type of the resource; schemas, organisations, sites or meters. | 
 **resourceId** | **Number**| ID of the resource. | 
 **start** | **String**| The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone. | 
 **end** | **String**| The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - format without a timezone. | [optional] 
 **aggregation** | **String**| Suggested aggregation of the data. Is not guaranteed if date range is too large for the aggregation type. 15 minute, day, month or year. | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getWaterStats

> Object getWaterStats(resourceType, resourceId, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.DataApi();
let resourceType = "resourceType_example"; // String | Type of the resource; schemas, organisations, sites or meters.
let resourceId = 56; // Number | ID of the resource.
let opts = {
  'start': 2018-12-03, // String | The date in ISO format - YYYY-MM-DD - without a timezone. If no date is provided, the data will default to lifetime.
  'end': 2018-12-03 // String | The date in ISO format - YYYY-MM-DD - without a timezone.
};
apiInstance.getWaterStats(resourceType, resourceId, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resourceType** | **String**| Type of the resource; schemas, organisations, sites or meters. | 
 **resourceId** | **Number**| ID of the resource. | 
 **start** | **String**| The date in ISO format - YYYY-MM-DD - without a timezone. If no date is provided, the data will default to lifetime. | [optional] 
 **end** | **String**| The date in ISO format - YYYY-MM-DD - without a timezone. | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

