# SolarSchools.CreateSavingsGuarantee

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startDate** | **String** | Inclusive start date of the guarantee in the site&#39;s timezone. | 
**endDate** | **String** | Exclusive end date of the guarantee in the site&#39;s timezone. | 
**energyTarget** | **Number** | Total target of the guarantee in watts. | 
**psh** | **Number** | Average daily psh required to meet the guarantee. | 
**status** | **Number** | Status of the savings guarantee; Enabled: 1, Disabled: 9. | [optional] 


