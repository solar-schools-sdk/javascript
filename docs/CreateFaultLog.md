# SolarSchools.CreateFaultLog

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**severity** | **Number** | Change in severity of the fault. | [optional] 
**status** | **Number** | Change in status of the fault. | [optional] 
**note** | **String** |  | 
**duration** | **Number** | Duration in minutes spent on this log. | [optional] 
**logTime** | **String** | Time of the log in the site&#39;s timezone. | [optional] 


