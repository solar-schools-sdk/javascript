# SolarSchools.LogEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**application** | **String** | Application the event occurred on. | [optional] 
**path** | **String** | Address or file path the event occurred. | [optional] 
**eventType** | **String** | Type of the event. | [optional] 
**severity** | **Number** | Severity of the event; Low: 1; Medium: 5; High: 8; Critical: 10. | [optional] 
**title** | **String** |  | [optional] 
**details** | **String** |  | [optional] 
**timeOccurred** | **String** | Time of the fault in the site&#39;s timezone. | [optional] 


