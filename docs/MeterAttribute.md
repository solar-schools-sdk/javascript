# SolarSchools.MeterAttribute

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attributeId** | **Number** | ID of the attribute. | 
**value** | **String** |  | 


