# SolarSchools.VirtualMeterChannel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelId** | **Number** |  | 
**multiplier** | **Number** |  | 


