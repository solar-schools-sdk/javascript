# SolarSchools.TestPermission

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**resourceType** | **String** | Type of the resource. | 
**resourceId** | **Number** | ID of the resource. | [optional] 
**intent** | **String** | Intent of the permission. | 
**write** | **Boolean** | Is write required. | 


