# SolarSchools.CreateOrganisationRequestData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**schemaId** | **Number** |  | 
**name** | **String** |  | 
**slug** | **String** |  | 
**description** | **String** |  | 
**status** | **Boolean** |  | 


