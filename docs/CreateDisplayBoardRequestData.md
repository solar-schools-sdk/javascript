# SolarSchools.CreateDisplayBoardRequestData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**resourceType** | **String** | Resource type. | 
**resourceId** | **String** | ID of the resource type. | 
**config** | [**Object**](.md) |  | [optional] 


