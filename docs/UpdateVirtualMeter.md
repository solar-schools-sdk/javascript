# SolarSchools.UpdateVirtualMeter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**groupLabel** | **String** |  | 
**parentId** | **Number** |  | [optional] 
**description** | **String** |  | 
**status** | **Number** |  | 


