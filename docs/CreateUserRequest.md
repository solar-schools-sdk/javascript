# SolarSchools.CreateUserRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**role** | **String** |  | 
**firstName** | **String** |  | 
**surname** | **String** |  | 
**phone** | **String** |  | [optional] 
**email** | **String** |  | 
**street** | **String** |  | [optional] 
**suburb** | **String** |  | [optional] 
**city** | **String** |  | [optional] 
**state** | **String** |  | [optional] 
**postcode** | **String** |  | [optional] 
**country** | **String** |  | [optional] 


