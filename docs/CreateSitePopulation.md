# SolarSchools.CreateSitePopulation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startDate** | **String** |  | [optional] 
**endDate** | **String** |  | [optional] 
**personCount** | **Number** |  | [optional] 


