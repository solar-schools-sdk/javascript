# SolarSchools.RefreshSession

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**refreshToken** | **String** |  | 
**username** | **String** |  | 


