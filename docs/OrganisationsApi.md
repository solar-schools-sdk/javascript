# SolarSchools.OrganisationsApi

All URIs are relative to *https://api.solarschools.net/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get**](OrganisationsApi.md#get) | **GET** /organisations/{id} | 
[**getMapSites**](OrganisationsApi.md#getMapSites) | **GET** /organisations/{id}/sites/map | 
[**getSchemas**](OrganisationsApi.md#getSchemas) | **GET** /organisations/{id}/schemas | 
[**getSiteCustomAttributes**](OrganisationsApi.md#getSiteCustomAttributes) | **GET** /organisations/{id}/sites/attributes | 
[**getSites**](OrganisationsApi.md#getSites) | **GET** /organisations/{id}/sites | 
[**getSolarCurrentStats**](OrganisationsApi.md#getSolarCurrentStats) | **GET** /organisations/{id}/solar/stats/current | 
[**getSummary**](OrganisationsApi.md#getSummary) | **GET** /organisations/{id}/summary | 
[**testSiteName**](OrganisationsApi.md#testSiteName) | **GET** /organisations/{id}/sites/test-name | 



## get

> Object get(id)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.OrganisationsApi();
let id = 56; // Number | 
apiInstance.get(id).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMapSites

> Object getMapSites(id, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.OrganisationsApi();
let id = 56; // Number | 
let opts = {
  'filter': "filter_example" // String | Filter for the name of the site.
};
apiInstance.getMapSites(id, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **filter** | **String**| Filter for the name of the site. | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSchemas

> Object getSchemas(id)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.OrganisationsApi();
let id = 56; // Number | 
apiInstance.getSchemas(id).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSiteCustomAttributes

> Object getSiteCustomAttributes(id)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiAuthorizer
let ApiAuthorizer = defaultClient.authentications['ApiAuthorizer'];
ApiAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.OrganisationsApi();
let id = 56; // Number | 
apiInstance.getSiteCustomAttributes(id).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

**Object**

### Authorization

[ApiAuthorizer](../README.md#ApiAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSites

> Object getSites(id, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.OrganisationsApi();
let id = 56; // Number | 
let opts = {
  'page': 56, // Number | Page of results to return.
  'limit': 56, // Number | Limit of results to return.
  'filter': "filter_example" // String | Filter for the organisation name of the site.
};
apiInstance.getSites(id, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **page** | **Number**| Page of results to return. | [optional] 
 **limit** | **Number**| Limit of results to return. | [optional] 
 **filter** | **String**| Filter for the organisation name of the site. | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSolarCurrentStats

> Object getSolarCurrentStats(id)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.OrganisationsApi();
let id = 56; // Number | 
apiInstance.getSolarCurrentStats(id).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSummary

> Object getSummary(id, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiSoftAuthorizer
let ApiSoftAuthorizer = defaultClient.authentications['ApiSoftAuthorizer'];
ApiSoftAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiSoftAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.OrganisationsApi();
let id = 56; // Number | 
let opts = {
  'timezone': Australia/Brisbane // String | User's IANA timezone.
};
apiInstance.getSummary(id, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **timezone** | **String**| User&#39;s IANA timezone. | [optional] 

### Return type

**Object**

### Authorization

[ApiSoftAuthorizer](../README.md#ApiSoftAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## testSiteName

> Object testSiteName(id, name, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiAuthorizer
let ApiAuthorizer = defaultClient.authentications['ApiAuthorizer'];
ApiAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.OrganisationsApi();
let id = 56; // Number | 
let name = "name_example"; // String | Name of the site to test.
let opts = {
  'slug': "slug_example" // String | Slug of the site to test.
};
apiInstance.testSiteName(id, name, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **name** | **String**| Name of the site to test. | 
 **slug** | **String**| Slug of the site to test. | [optional] 

### Return type

**Object**

### Authorization

[ApiAuthorizer](../README.md#ApiAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

