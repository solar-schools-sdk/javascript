# SolarSchools.UpdateWattwatchersDeviceChannel

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ctRating** | **Number** | Total capacity of the current transformer. | [optional] 
**label** | **String** |  | 
**categoryId** | **Number** |  | 


