# SolarSchools.UpdateSite

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**organisationId** | **Number** |  | 
**name** | **String** |  | 
**slug** | **String** |  | 
**description** | **String** |  | [optional] 
**street** | **String** |  | 
**suburb** | **String** |  | [optional] 
**city** | **String** |  | 
**region** | **String** |  | [optional] 
**postcode** | **String** |  | [optional] 
**countryDivisionId** | **Number** |  | 
**latitude** | **Number** |  | 
**longitude** | **Number** |  | 
**weatherLocationId** | **Number** |  | [optional] 
**hasGridData** | **Number** |  | [optional] 
**hasSolarData** | **Number** |  | [optional] 
**status** | **Number** |  | 
**customAttributes** | **{String: String}** | Dynamic collection of custom attributes required for the site. | [optional] 


