# SolarSchools.Channel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelId** | **Number** | Channel/source ID or number/position on the physical meter. | 
**name** | **String** |  | 
**type** | **Number** |  | 
**ctCapacity** | **Number** | Total capacity of the current transformer. | [optional] 
**multiplier** | **Number** | Multiplier for when only 1 phase is monitored on a multi-phase system. | 
**isReversed** | **Boolean** | Is the CT clamp installed backwards so the data needs to be reversed? | 
**aggregateData** | **Boolean** | Should the data collected on this channel be aggregated? | 
**status** | **Number** |  | 


