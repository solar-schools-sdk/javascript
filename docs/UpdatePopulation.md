# SolarSchools.UpdatePopulation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startDate** | **String** | Inclusive start date of the population in the site&#39;s timezone. | 
**endDate** | **String** | Exclusive end date of the population in the site&#39;s timezone. | 
**personCount** | **Number** | Person count at the site within the time range. | 


