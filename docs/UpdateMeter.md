# SolarSchools.UpdateMeter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**utility** | **Number** |  | 
**meterTypeId** | **Number** |  | [optional] 
**deviceId** | **String** |  | 
**location** | **String** |  | [optional] 
**description** | **String** |  | 
**notes** | **String** |  | [optional] 
**installerId** | **String** |  | 
**channelCount** | **Number** | Number of physical channels on the meter. | 
**status** | **Number** |  | 
**firstDataTime** | **String** | Date and time of when the first data is available for the meter in UTC. Must be provided for the meter to be enabled. | [optional] 
**customAttributes** | **{String: Object}** | Dynamic collection of custom attributes required for the meter type. | [optional] 


