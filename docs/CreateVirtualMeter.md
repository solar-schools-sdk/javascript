# SolarSchools.CreateVirtualMeter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**siteId** | **Number** |  | 
**name** | **String** |  | 
**groupLabel** | **String** |  | 
**parentId** | **Number** |  | [optional] 
**typeId** | **Number** |  | 
**description** | **String** |  | 
**status** | **Number** |  | 
**channels** | [**[VirtualMeterChannel]**](VirtualMeterChannel.md) |  | [optional] 


