# SolarSchools.UpdateUserRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**role** | **String** |  | [optional] 
**firstName** | **String** |  | [optional] 
**surname** | **String** |  | [optional] 
**phone** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**street** | **String** |  | [optional] 
**suburb** | **String** |  | [optional] 
**city** | **String** |  | [optional] 
**state** | **String** |  | [optional] 
**postcode** | **String** |  | [optional] 
**country** | **String** |  | [optional] 


