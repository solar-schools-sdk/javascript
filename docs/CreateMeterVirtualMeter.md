# SolarSchools.CreateMeterVirtualMeter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**type** | **Number** |  | 
**description** | **String** |  | 
**aggregateData** | **Boolean** | Should the data collected on this channel be aggregated? | 
**status** | **Number** |  | 
**channels** | [**[CreateMeterVirtualMeterChannel]**](CreateMeterVirtualMeterChannel.md) |  | 
**solarSystem** | [**CreateMeterSolarSystem**](CreateMeterSolarSystem.md) |  | [optional] 


