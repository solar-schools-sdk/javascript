# SolarSchools.LoginRequestData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **String** |  | 
**password** | **String** |  | 
**resource** | **String** |  | [optional] 


