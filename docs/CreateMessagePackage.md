# SolarSchools.CreateMessagePackage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**siteId** | **Number** |  | 
**userId** | **Number** |  | [optional] 
**resourceType** | **Number** |  | [optional] 
**resourceId** | **Number** |  | [optional] 
**referenceUserId** | **Number** |  | [optional] 
**type** | **Number** |  | 
**eventDate** | **String** | Date of the event. Must be in ISO format; e.g.: 2020-01-13. | 
**duration** | **String** | Time spent on the event. Must be in format: &#39;hh:mm&#39;. | [optional] 
**message** | **String** |  | 



## Enum: ResourceTypeEnum


* `1` (value: `1`)

* `2` (value: `2`)

* `3` (value: `3`)

* `4` (value: `4`)





## Enum: TypeEnum


* `1` (value: `1`)

* `2` (value: `2`)

* `3` (value: `3`)

* `4` (value: `4`)

* `5` (value: `5`)

* `6` (value: `6`)

* `7` (value: `7`)




