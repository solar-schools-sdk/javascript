# SolarSchools.CreateOrganisation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**schemaId** | **Number** |  | 
**name** | **String** |  | 
**slug** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**status** | **Number** | Status of the organisation; Enabled: 1, Disabled: 9. | 
**additionalSchemas** | **[Number]** |  | [optional] 


