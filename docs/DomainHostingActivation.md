# SolarSchools.DomainHostingActivation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**domainId** | **String** | ID of the DNS hosting zone for internally-hosted domains. | [optional] 
**hostingInstructions** | **String** | Details on how to make the domain live. | [optional] 


