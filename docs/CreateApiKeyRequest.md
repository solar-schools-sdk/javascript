# SolarSchools.CreateApiKeyRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **Number** |  | 
**name** | **String** |  | 
**status** | **Number** |  | 
**domains** | **[String]** |  | 


