# SolarSchools.SitesApi

All URIs are relative to *https://api.solarschools.net/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**callDelete**](SitesApi.md#callDelete) | **DELETE** /sites/{id} | 
[**get**](SitesApi.md#get) | **GET** /sites/{id} | 
[**getEnergyBreakdown**](SitesApi.md#getEnergyBreakdown) | **GET** /sites/{id}/energy/breakdown | 
[**getEnergyDataFeed**](SitesApi.md#getEnergyDataFeed) | **GET** /sites/{id}/energy/data-feed | 
[**getEnergyLifetimeData**](SitesApi.md#getEnergyLifetimeData) | **GET** /sites/{id}/energy/lifetime-data | 
[**getMeters**](SitesApi.md#getMeters) | **GET** /sites/{id}/meters | 
[**getPhysicalMeters**](SitesApi.md#getPhysicalMeters) | **GET** /sites/{id}/meters/physical | 
[**getSummary**](SitesApi.md#getSummary) | **GET** /sites/{id}/summary | 
[**getSupportNotes**](SitesApi.md#getSupportNotes) | **GET** /sites/{id}/support-notes | 
[**getSystems**](SitesApi.md#getSystems) | **GET** /sites/{id}/systems | 
[**getSystemsDataFeed**](SitesApi.md#getSystemsDataFeed) | **GET** /sites/{id}/systems/data-feed | 
[**getVirtualMeters**](SitesApi.md#getVirtualMeters) | **GET** /sites/{id}/meters/virtual | 
[**getWeather**](SitesApi.md#getWeather) | **GET** /sites/{id}/weather | 
[**update**](SitesApi.md#update) | **PUT** /sites/{id} | 



## callDelete

> Object callDelete(id)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiAuthorizer
let ApiAuthorizer = defaultClient.authentications['ApiAuthorizer'];
ApiAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.SitesApi();
let id = 56; // Number | 
apiInstance.callDelete(id).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

**Object**

### Authorization

[ApiAuthorizer](../README.md#ApiAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get

> Object get(id, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.SitesApi();
let id = 56; // Number | 
let opts = {
  'timezone': Australia/Brisbane, // String | IANA timezone of the caller to determine the contextual current day. Defaults to Australia/Brisbane
  'extendedDetails': true, // Boolean | Flag to attach extended details such as organisation details and basic stats.
  'lastEnergy': true, // Boolean | Flag to attach the last energy stats.
  'summary': true, // Boolean | Flag to attach energy summary stats.
  'weather': true, // Boolean | Flag to attach current weather for the site.
  'customAttributes': true // Boolean | Flag to attach the site's custom attributes.
};
apiInstance.get(id, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **timezone** | **String**| IANA timezone of the caller to determine the contextual current day. Defaults to Australia/Brisbane | [optional] 
 **extendedDetails** | **Boolean**| Flag to attach extended details such as organisation details and basic stats. | [optional] 
 **lastEnergy** | **Boolean**| Flag to attach the last energy stats. | [optional] 
 **summary** | **Boolean**| Flag to attach energy summary stats. | [optional] 
 **weather** | **Boolean**| Flag to attach current weather for the site. | [optional] 
 **customAttributes** | **Boolean**| Flag to attach the site&#39;s custom attributes. | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getEnergyBreakdown

> Object getEnergyBreakdown(id, start, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.SitesApi();
let id = 56; // Number | 
let start = 2018-12-03 or 2018-12-03 13:00:00 or 2018-12-03T13:00:00; // String | The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone.
let opts = {
  'end': 2018-12-03 or 2018-12-03 13:00:00 or 2018-12-03T13:00:00, // String | The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone.
  'aggregation': month or 30 minute, // String | Suggested aggregation of the data. Is not guaranteed if date range is too large for the aggregation type.
  'grouping': "grouping_example" // String | Grouping of the submeters breakdown; type, group or none; default: none.
};
apiInstance.getEnergyBreakdown(id, start, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **start** | **String**| The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone. | 
 **end** | **String**| The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone. | [optional] 
 **aggregation** | **String**| Suggested aggregation of the data. Is not guaranteed if date range is too large for the aggregation type. | [optional] 
 **grouping** | **String**| Grouping of the submeters breakdown; type, group or none; default: none. | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getEnergyDataFeed

> Object getEnergyDataFeed(id, start, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.SitesApi();
let id = 56; // Number | 
let start = 2018-12-03 or 2018-12-03 13:00:00 or 2018-12-03T13:00:00; // String | The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone.
let opts = {
  'end': 2018-12-03 or 2018-12-03 13:00:00 or 2018-12-03T13:00:00, // String | The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone.
  'aggregation': month or 30 minute // String | Suggested aggregation of the data. Is not guaranteed if date range is too large for the aggregation type.
};
apiInstance.getEnergyDataFeed(id, start, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **start** | **String**| The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone. | 
 **end** | **String**| The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone. | [optional] 
 **aggregation** | **String**| Suggested aggregation of the data. Is not guaranteed if date range is too large for the aggregation type. | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getEnergyLifetimeData

> Object getEnergyLifetimeData(id)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.SitesApi();
let id = 56; // Number | 
apiInstance.getEnergyLifetimeData(id).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getMeters

> Object getMeters(id)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiAuthorizer
let ApiAuthorizer = defaultClient.authentications['ApiAuthorizer'];
ApiAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.SitesApi();
let id = 56; // Number | 
apiInstance.getMeters(id).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

**Object**

### Authorization

[ApiAuthorizer](../README.md#ApiAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getPhysicalMeters

> Object getPhysicalMeters(id, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiAuthorizer
let ApiAuthorizer = defaultClient.authentications['ApiAuthorizer'];
ApiAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.SitesApi();
let id = 56; // Number | 
let opts = {
  'utility': 56, // Number | Utility type of the meter.
  'types': [null], // [Number] | Filter of the type of the physical meter.
  'excludeTypes': [null], // [Number] | Filter of the types of the physical meter to exclude.
  'status': 56 // Number | Filter of the status of the physical meter.
};
apiInstance.getPhysicalMeters(id, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **utility** | **Number**| Utility type of the meter. | [optional] 
 **types** | [**[Number]**](Number.md)| Filter of the type of the physical meter. | [optional] 
 **excludeTypes** | [**[Number]**](Number.md)| Filter of the types of the physical meter to exclude. | [optional] 
 **status** | **Number**| Filter of the status of the physical meter. | [optional] 

### Return type

**Object**

### Authorization

[ApiAuthorizer](../README.md#ApiAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSummary

> Object getSummary(id, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiSoftAuthorizer
let ApiSoftAuthorizer = defaultClient.authentications['ApiSoftAuthorizer'];
ApiSoftAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiSoftAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.SitesApi();
let id = 56; // Number | 
let opts = {
  'timezone': Australia/Brisbane // String | The end-users IANA timezone.
};
apiInstance.getSummary(id, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **timezone** | **String**| The end-users IANA timezone. | [optional] 

### Return type

**Object**

### Authorization

[ApiSoftAuthorizer](../README.md#ApiSoftAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSupportNotes

> Object getSupportNotes(id, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiAuthorizer
let ApiAuthorizer = defaultClient.authentications['ApiAuthorizer'];
ApiAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.SitesApi();
let id = 56; // Number | 
let opts = {
  'type': [null], // [Number] | List of note types to filter by.
  'page': 56, // Number | Page of results to return.
  'limit': 56, // Number | Limit of results to return.
  'order': "order_example", // String | Ordering of the results; asc or desc.
  'orderBy': "orderBy_example" // String | Column to order the results by; eventDate, id, resourceType, or created.
};
apiInstance.getSupportNotes(id, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **type** | [**[Number]**](Number.md)| List of note types to filter by. | [optional] 
 **page** | **Number**| Page of results to return. | [optional] 
 **limit** | **Number**| Limit of results to return. | [optional] 
 **order** | **String**| Ordering of the results; asc or desc. | [optional] 
 **orderBy** | **String**| Column to order the results by; eventDate, id, resourceType, or created. | [optional] 

### Return type

**Object**

### Authorization

[ApiAuthorizer](../README.md#ApiAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSystems

> Object getSystems(id)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.SitesApi();
let id = 56; // Number | 
apiInstance.getSystems(id).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getSystemsDataFeed

> Object getSystemsDataFeed(id, start, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.SitesApi();
let id = 56; // Number | 
let start = 2018-12-03 or 2018-12-03 13:00:00 or 2018-12-03T13:00:00; // String | The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone.
let opts = {
  'end': 2018-12-03 or 2018-12-03 13:00:00 or 2018-12-03T13:00:00, // String | The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone.
  'includeDisabled': true // Boolean | Include disabled virtual meter data sources.
};
apiInstance.getSystemsDataFeed(id, start, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **start** | **String**| The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone. | 
 **end** | **String**| The date and time in ISO format - YYYY-MM-DD or YYYY-MM-DD HH:mm:ss or YYYY-MM-DDTHH:mm:ss - without a timezone. | [optional] 
 **includeDisabled** | **Boolean**| Include disabled virtual meter data sources. | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getVirtualMeters

> Object getVirtualMeters(id, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.SitesApi();
let id = 56; // Number | 
let opts = {
  'parentId': 56, // Number | ID of the parent.
  'types': [null], // [Number] | Filter of the type of the virtual meter.
  'excludeTypes': [null], // [Number] | Filter of the types of the virtual meter to exclude.
  'nodeTypes': ["null"], // [String] | Type of nodes to match: grid, generator or battery.
  'excludeNodeTypes': ["null"], // [String] | Type of nodes to not match: grid, generator or battery.
  'status': 56 // Number | Filter of the status of the virtual meter.
};
apiInstance.getVirtualMeters(id, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **parentId** | **Number**| ID of the parent. | [optional] 
 **types** | [**[Number]**](Number.md)| Filter of the type of the virtual meter. | [optional] 
 **excludeTypes** | [**[Number]**](Number.md)| Filter of the types of the virtual meter to exclude. | [optional] 
 **nodeTypes** | [**[String]**](String.md)| Type of nodes to match: grid, generator or battery. | [optional] 
 **excludeNodeTypes** | [**[String]**](String.md)| Type of nodes to not match: grid, generator or battery. | [optional] 
 **status** | **Number**| Filter of the status of the virtual meter. | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## getWeather

> Object getWeather(id, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';

let apiInstance = new SolarSchools.SitesApi();
let id = 56; // Number | 
let opts = {
  'date': 2018-12-03 // String | The requested date for the weather.
};
apiInstance.getWeather(id, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **date** | **String**| The requested date for the weather. | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## update

> Object update(id, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiAuthorizer
let ApiAuthorizer = defaultClient.authentications['ApiAuthorizer'];
ApiAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.SitesApi();
let id = 56; // Number | 
let opts = {
  'updateSite': new SolarSchools.UpdateSite() // UpdateSite | Payload for updating an existing site.
};
apiInstance.update(id, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **updateSite** | [**UpdateSite**](UpdateSite.md)| Payload for updating an existing site. | [optional] 

### Return type

**Object**

### Authorization

[ApiAuthorizer](../README.md#ApiAuthorizer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

