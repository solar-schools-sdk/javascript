# SolarSchools.ContractorsApi

All URIs are relative to *https://api.solarschools.net/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**callDelete**](ContractorsApi.md#callDelete) | **DELETE** /contractors/{id} | 
[**create**](ContractorsApi.md#create) | **POST** /contractors/ | 
[**get**](ContractorsApi.md#get) | **GET** /contractors/{id} | 
[**list**](ContractorsApi.md#list) | **GET** /contractors/ | 
[**update**](ContractorsApi.md#update) | **PUT** /contractors/{id} | 



## callDelete

> Object callDelete(id)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiAuthorizer
let ApiAuthorizer = defaultClient.authentications['ApiAuthorizer'];
ApiAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.ContractorsApi();
let id = 56; // Number | 
apiInstance.callDelete(id).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

**Object**

### Authorization

[ApiAuthorizer](../README.md#ApiAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## create

> Object create(opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiAuthorizer
let ApiAuthorizer = defaultClient.authentications['ApiAuthorizer'];
ApiAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.ContractorsApi();
let opts = {
  'createContractor': new SolarSchools.CreateContractor() // CreateContractor | Payload for creating a new contractor.
};
apiInstance.create(opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createContractor** | [**CreateContractor**](CreateContractor.md)| Payload for creating a new contractor. | [optional] 

### Return type

**Object**

### Authorization

[ApiAuthorizer](../README.md#ApiAuthorizer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## get

> Object get(id)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiAuthorizer
let ApiAuthorizer = defaultClient.authentications['ApiAuthorizer'];
ApiAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.ContractorsApi();
let id = 56; // Number | 
apiInstance.get(id).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

**Object**

### Authorization

[ApiAuthorizer](../README.md#ApiAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## list

> Object list()



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiAuthorizer
let ApiAuthorizer = defaultClient.authentications['ApiAuthorizer'];
ApiAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.ContractorsApi();
apiInstance.list().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[ApiAuthorizer](../README.md#ApiAuthorizer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## update

> Object update(id, opts)



### Example

```javascript
import SolarSchools from '@solarschools/education-sdk';
let defaultClient = SolarSchools.ApiClient.instance;
// Configure API key authorization: ApiAuthorizer
let ApiAuthorizer = defaultClient.authentications['ApiAuthorizer'];
ApiAuthorizer.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//ApiAuthorizer.apiKeyPrefix = 'Token';

let apiInstance = new SolarSchools.ContractorsApi();
let id = 56; // Number | 
let opts = {
  'updateContractor': new SolarSchools.UpdateContractor() // UpdateContractor | Payload for updating an existing contractor.
};
apiInstance.update(id, opts).then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **updateContractor** | [**UpdateContractor**](UpdateContractor.md)| Payload for updating an existing contractor. | [optional] 

### Return type

**Object**

### Authorization

[ApiAuthorizer](../README.md#ApiAuthorizer)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

