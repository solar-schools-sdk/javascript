/**
 * Solar Schools - Public API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * Contact: dev@solarschools.net
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.SolarSchools);
  }
}(this, function(expect, SolarSchools) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new SolarSchools.DomainHostingActivation();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('DomainHostingActivation', function() {
    it('should create an instance of DomainHostingActivation', function() {
      // uncomment below and update the code to test DomainHostingActivation
      //var instance = new SolarSchools.DomainHostingActivation();
      //expect(instance).to.be.a(SolarSchools.DomainHostingActivation);
    });

    it('should have the property domainId (base name: "domainId")', function() {
      // uncomment below and update the code to test the property domainId
      //var instance = new SolarSchools.DomainHostingActivation();
      //expect(instance).to.be();
    });

    it('should have the property hostingInstructions (base name: "hostingInstructions")', function() {
      // uncomment below and update the code to test the property hostingInstructions
      //var instance = new SolarSchools.DomainHostingActivation();
      //expect(instance).to.be();
    });

  });

}));
