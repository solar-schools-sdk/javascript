/**
 * Solar Schools - Public API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * Contact: dev@solarschools.net
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The UpdateMessage model module.
 * @module model/UpdateMessage
 * @version 0.0.0
 */
class UpdateMessage {
    /**
     * Constructs a new <code>UpdateMessage</code>.
     * @alias module:model/UpdateMessage
     * @param type {Number} 
     * @param eventDate {String} Date of the event. Must be in ISO format; e.g.: 2020-01-13.
     * @param message {String} 
     */
    constructor(type, eventDate, message) { 
        
        UpdateMessage.initialize(this, type, eventDate, message);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, type, eventDate, message) { 
        obj['type'] = type;
        obj['eventDate'] = eventDate;
        obj['message'] = message;
    }

    /**
     * Constructs a <code>UpdateMessage</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UpdateMessage} obj Optional instance to populate.
     * @return {module:model/UpdateMessage} The populated <code>UpdateMessage</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new UpdateMessage();

            if (data.hasOwnProperty('userId')) {
                obj['userId'] = ApiClient.convertToType(data['userId'], 'Number');
            }
            if (data.hasOwnProperty('resourceType')) {
                obj['resourceType'] = ApiClient.convertToType(data['resourceType'], 'Number');
            }
            if (data.hasOwnProperty('resourceId')) {
                obj['resourceId'] = ApiClient.convertToType(data['resourceId'], 'Number');
            }
            if (data.hasOwnProperty('referenceUserId')) {
                obj['referenceUserId'] = ApiClient.convertToType(data['referenceUserId'], 'Number');
            }
            if (data.hasOwnProperty('type')) {
                obj['type'] = ApiClient.convertToType(data['type'], 'Number');
            }
            if (data.hasOwnProperty('eventDate')) {
                obj['eventDate'] = ApiClient.convertToType(data['eventDate'], 'String');
            }
            if (data.hasOwnProperty('duration')) {
                obj['duration'] = ApiClient.convertToType(data['duration'], 'String');
            }
            if (data.hasOwnProperty('message')) {
                obj['message'] = ApiClient.convertToType(data['message'], 'String');
            }
        }
        return obj;
    }

/**
     * @return {Number}
     */
    getUserId() {
        return this.userId;
    }

    /**
     * @param {Number} userId
     */
    setUserId(userId) {
        this['userId'] = userId;
    }
/**
     * minimum: 1
     * maximum: 4
     * @return {Number}
     */
    getResourceType() {
        return this.resourceType;
    }

    /**
     * @param {Number} resourceType
     */
    setResourceType(resourceType) {
        this['resourceType'] = resourceType;
    }
/**
     * @return {Number}
     */
    getResourceId() {
        return this.resourceId;
    }

    /**
     * @param {Number} resourceId
     */
    setResourceId(resourceId) {
        this['resourceId'] = resourceId;
    }
/**
     * @return {Number}
     */
    getReferenceUserId() {
        return this.referenceUserId;
    }

    /**
     * @param {Number} referenceUserId
     */
    setReferenceUserId(referenceUserId) {
        this['referenceUserId'] = referenceUserId;
    }
/**
     * minimum: 1
     * maximum: 7
     * @return {Number}
     */
    getType() {
        return this.type;
    }

    /**
     * @param {Number} type
     */
    setType(type) {
        this['type'] = type;
    }
/**
     * Returns Date of the event. Must be in ISO format; e.g.: 2020-01-13.
     * @return {String}
     */
    getEventDate() {
        return this.eventDate;
    }

    /**
     * Sets Date of the event. Must be in ISO format; e.g.: 2020-01-13.
     * @param {String} eventDate Date of the event. Must be in ISO format; e.g.: 2020-01-13.
     */
    setEventDate(eventDate) {
        this['eventDate'] = eventDate;
    }
/**
     * Returns Time spent on the event. Must be in format: 'hh:mm'.
     * @return {String}
     */
    getDuration() {
        return this.duration;
    }

    /**
     * Sets Time spent on the event. Must be in format: 'hh:mm'.
     * @param {String} duration Time spent on the event. Must be in format: 'hh:mm'.
     */
    setDuration(duration) {
        this['duration'] = duration;
    }
/**
     * @return {String}
     */
    getMessage() {
        return this.message;
    }

    /**
     * @param {String} message
     */
    setMessage(message) {
        this['message'] = message;
    }

}

/**
 * @member {Number} userId
 */
UpdateMessage.prototype['userId'] = undefined;

/**
 * @member {Number} resourceType
 */
UpdateMessage.prototype['resourceType'] = undefined;

/**
 * @member {Number} resourceId
 */
UpdateMessage.prototype['resourceId'] = undefined;

/**
 * @member {Number} referenceUserId
 */
UpdateMessage.prototype['referenceUserId'] = undefined;

/**
 * @member {Number} type
 */
UpdateMessage.prototype['type'] = undefined;

/**
 * Date of the event. Must be in ISO format; e.g.: 2020-01-13.
 * @member {String} eventDate
 */
UpdateMessage.prototype['eventDate'] = undefined;

/**
 * Time spent on the event. Must be in format: 'hh:mm'.
 * @member {String} duration
 */
UpdateMessage.prototype['duration'] = undefined;

/**
 * @member {String} message
 */
UpdateMessage.prototype['message'] = undefined;






export default UpdateMessage;

