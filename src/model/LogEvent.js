/**
 * Solar Schools - Public API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * Contact: dev@solarschools.net
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The LogEvent model module.
 * @module model/LogEvent
 * @version 0.0.0
 */
class LogEvent {
    /**
     * Constructs a new <code>LogEvent</code>.
     * @alias module:model/LogEvent
     */
    constructor() { 
        
        LogEvent.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>LogEvent</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/LogEvent} obj Optional instance to populate.
     * @return {module:model/LogEvent} The populated <code>LogEvent</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new LogEvent();

            if (data.hasOwnProperty('application')) {
                obj['application'] = ApiClient.convertToType(data['application'], 'String');
            }
            if (data.hasOwnProperty('path')) {
                obj['path'] = ApiClient.convertToType(data['path'], 'String');
            }
            if (data.hasOwnProperty('eventType')) {
                obj['eventType'] = ApiClient.convertToType(data['eventType'], 'String');
            }
            if (data.hasOwnProperty('severity')) {
                obj['severity'] = ApiClient.convertToType(data['severity'], 'Number');
            }
            if (data.hasOwnProperty('title')) {
                obj['title'] = ApiClient.convertToType(data['title'], 'String');
            }
            if (data.hasOwnProperty('details')) {
                obj['details'] = ApiClient.convertToType(data['details'], 'String');
            }
            if (data.hasOwnProperty('timeOccurred')) {
                obj['timeOccurred'] = ApiClient.convertToType(data['timeOccurred'], 'String');
            }
        }
        return obj;
    }

    /**
     * Validates the JSON data with respect to <code>LogEvent</code>.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @return {boolean} to indicate whether the JSON data is valid with respect to <code>LogEvent</code>.
     */
    static validateJSON(data) {
        // ensure the json data is a string
        if (data['application'] && !(typeof data['application'] === 'string' || data['application'] instanceof String)) {
            throw new Error("Expected the field `application` to be a primitive type in the JSON string but got " + data['application']);
        }
        // ensure the json data is a string
        if (data['path'] && !(typeof data['path'] === 'string' || data['path'] instanceof String)) {
            throw new Error("Expected the field `path` to be a primitive type in the JSON string but got " + data['path']);
        }
        // ensure the json data is a string
        if (data['eventType'] && !(typeof data['eventType'] === 'string' || data['eventType'] instanceof String)) {
            throw new Error("Expected the field `eventType` to be a primitive type in the JSON string but got " + data['eventType']);
        }
        // ensure the json data is a string
        if (data['title'] && !(typeof data['title'] === 'string' || data['title'] instanceof String)) {
            throw new Error("Expected the field `title` to be a primitive type in the JSON string but got " + data['title']);
        }
        // ensure the json data is a string
        if (data['details'] && !(typeof data['details'] === 'string' || data['details'] instanceof String)) {
            throw new Error("Expected the field `details` to be a primitive type in the JSON string but got " + data['details']);
        }
        // ensure the json data is a string
        if (data['timeOccurred'] && !(typeof data['timeOccurred'] === 'string' || data['timeOccurred'] instanceof String)) {
            throw new Error("Expected the field `timeOccurred` to be a primitive type in the JSON string but got " + data['timeOccurred']);
        }

        return true;
    }

/**
     * Returns Application the event occurred on.
     * @return {String}
     */
    getApplication() {
        return this.application;
    }

    /**
     * Sets Application the event occurred on.
     * @param {String} application Application the event occurred on.
     */
    setApplication(application) {
        this['application'] = application;
    }
/**
     * Returns Address or file path the event occurred.
     * @return {String}
     */
    getPath() {
        return this.path;
    }

    /**
     * Sets Address or file path the event occurred.
     * @param {String} path Address or file path the event occurred.
     */
    setPath(path) {
        this['path'] = path;
    }
/**
     * Returns Type of the event.
     * @return {String}
     */
    getEventType() {
        return this.eventType;
    }

    /**
     * Sets Type of the event.
     * @param {String} eventType Type of the event.
     */
    setEventType(eventType) {
        this['eventType'] = eventType;
    }
/**
     * Returns Severity of the event; Low: 1; Medium: 5; High: 8; Critical: 10.
     * @return {Number}
     */
    getSeverity() {
        return this.severity;
    }

    /**
     * Sets Severity of the event; Low: 1; Medium: 5; High: 8; Critical: 10.
     * @param {Number} severity Severity of the event; Low: 1; Medium: 5; High: 8; Critical: 10.
     */
    setSeverity(severity) {
        this['severity'] = severity;
    }
/**
     * @return {String}
     */
    getTitle() {
        return this.title;
    }

    /**
     * @param {String} title
     */
    setTitle(title) {
        this['title'] = title;
    }
/**
     * @return {String}
     */
    getDetails() {
        return this.details;
    }

    /**
     * @param {String} details
     */
    setDetails(details) {
        this['details'] = details;
    }
/**
     * Returns Time of the fault in the site's timezone.
     * @return {String}
     */
    getTimeOccurred() {
        return this.timeOccurred;
    }

    /**
     * Sets Time of the fault in the site's timezone.
     * @param {String} timeOccurred Time of the fault in the site's timezone.
     */
    setTimeOccurred(timeOccurred) {
        this['timeOccurred'] = timeOccurred;
    }

}



/**
 * Application the event occurred on.
 * @member {String} application
 */
LogEvent.prototype['application'] = undefined;

/**
 * Address or file path the event occurred.
 * @member {String} path
 */
LogEvent.prototype['path'] = undefined;

/**
 * Type of the event.
 * @member {String} eventType
 */
LogEvent.prototype['eventType'] = undefined;

/**
 * Severity of the event; Low: 1; Medium: 5; High: 8; Critical: 10.
 * @member {Number} severity
 */
LogEvent.prototype['severity'] = undefined;

/**
 * @member {String} title
 */
LogEvent.prototype['title'] = undefined;

/**
 * @member {String} details
 */
LogEvent.prototype['details'] = undefined;

/**
 * Time of the fault in the site's timezone.
 * @member {String} timeOccurred
 */
LogEvent.prototype['timeOccurred'] = undefined;






export default LogEvent;

