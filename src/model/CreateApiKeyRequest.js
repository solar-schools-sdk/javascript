/**
 * Solar Schools - Public API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * Contact: dev@solarschools.net
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The CreateApiKeyRequest model module.
 * @module model/CreateApiKeyRequest
 * @version 0.0.0
 */
class CreateApiKeyRequest {
    /**
     * Constructs a new <code>CreateApiKeyRequest</code>.
     * @alias module:model/CreateApiKeyRequest
     * @param userId {Number} 
     * @param name {String} 
     * @param status {Number} 
     * @param domains {Array.<String>} 
     */
    constructor(userId, name, status, domains) { 
        
        CreateApiKeyRequest.initialize(this, userId, name, status, domains);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, userId, name, status, domains) { 
        obj['userId'] = userId;
        obj['name'] = name;
        obj['status'] = status;
        obj['domains'] = domains;
    }

    /**
     * Constructs a <code>CreateApiKeyRequest</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateApiKeyRequest} obj Optional instance to populate.
     * @return {module:model/CreateApiKeyRequest} The populated <code>CreateApiKeyRequest</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new CreateApiKeyRequest();

            if (data.hasOwnProperty('userId')) {
                obj['userId'] = ApiClient.convertToType(data['userId'], 'Number');
            }
            if (data.hasOwnProperty('name')) {
                obj['name'] = ApiClient.convertToType(data['name'], 'String');
            }
            if (data.hasOwnProperty('status')) {
                obj['status'] = ApiClient.convertToType(data['status'], 'Number');
            }
            if (data.hasOwnProperty('domains')) {
                obj['domains'] = ApiClient.convertToType(data['domains'], ['String']);
            }
        }
        return obj;
    }

/**
     * @return {Number}
     */
    getUserId() {
        return this.userId;
    }

    /**
     * @param {Number} userId
     */
    setUserId(userId) {
        this['userId'] = userId;
    }
/**
     * @return {String}
     */
    getName() {
        return this.name;
    }

    /**
     * @param {String} name
     */
    setName(name) {
        this['name'] = name;
    }
/**
     * @return {Number}
     */
    getStatus() {
        return this.status;
    }

    /**
     * @param {Number} status
     */
    setStatus(status) {
        this['status'] = status;
    }
/**
     * @return {Array.<String>}
     */
    getDomains() {
        return this.domains;
    }

    /**
     * @param {Array.<String>} domains
     */
    setDomains(domains) {
        this['domains'] = domains;
    }

}

/**
 * @member {Number} userId
 */
CreateApiKeyRequest.prototype['userId'] = undefined;

/**
 * @member {String} name
 */
CreateApiKeyRequest.prototype['name'] = undefined;

/**
 * @member {Number} status
 */
CreateApiKeyRequest.prototype['status'] = undefined;

/**
 * @member {Array.<String>} domains
 */
CreateApiKeyRequest.prototype['domains'] = undefined;






export default CreateApiKeyRequest;

