/**
 * Solar Schools - Public API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * Contact: dev@solarschools.net
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import MeterChannel from './MeterChannel';

/**
 * The SetChannels model module.
 * @module model/SetChannels
 * @version 0.0.0
 */
class SetChannels {
    /**
     * Constructs a new <code>SetChannels</code>.
     * @alias module:model/SetChannels
     */
    constructor() { 
        
        SetChannels.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>SetChannels</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/SetChannels} obj Optional instance to populate.
     * @return {module:model/SetChannels} The populated <code>SetChannels</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new SetChannels();

            if (data.hasOwnProperty('channels')) {
                obj['channels'] = ApiClient.convertToType(data['channels'], [MeterChannel]);
            }
        }
        return obj;
    }

    /**
     * Validates the JSON data with respect to <code>SetChannels</code>.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @return {boolean} to indicate whether the JSON data is valid with respect to <code>SetChannels</code>.
     */
    static validateJSON(data) {
        if (data['channels']) { // data not null
            // ensure the json data is an array
            if (!Array.isArray(data['channels'])) {
                throw new Error("Expected the field `channels` to be an array in the JSON data but got " + data['channels']);
            }
            // validate the optional field `channels` (array)
            for (const item of data['channels']) {
                MeterChannel.validateJsonObject(item);
            };
        }

        return true;
    }

/**
     * @return {Array.<module:model/MeterChannel>}
     */
    getChannels() {
        return this.channels;
    }

    /**
     * @param {Array.<module:model/MeterChannel>} channels
     */
    setChannels(channels) {
        this['channels'] = channels;
    }

}



/**
 * @member {Array.<module:model/MeterChannel>} channels
 */
SetChannels.prototype['channels'] = undefined;






export default SetChannels;

