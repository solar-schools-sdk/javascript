/**
 * Solar Schools - Public API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 * Contact: dev@solarschools.net.au
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The ResetPasswordPackage model module.
 * @module model/ResetPasswordPackage
 * @version 0.0.0
 */
class ResetPasswordPackage {
    /**
     * Constructs a new <code>ResetPasswordPackage</code>.
     * @alias module:model/ResetPasswordPackage
     * @param email {String} 
     * @param token {String} 
     * @param password {String} 
     */
    constructor(email, token, password) { 
        
        ResetPasswordPackage.initialize(this, email, token, password);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, email, token, password) { 
        obj['email'] = email;
        obj['token'] = token;
        obj['password'] = password;
    }

    /**
     * Constructs a <code>ResetPasswordPackage</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ResetPasswordPackage} obj Optional instance to populate.
     * @return {module:model/ResetPasswordPackage} The populated <code>ResetPasswordPackage</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new ResetPasswordPackage();

            if (data.hasOwnProperty('email')) {
                obj['email'] = ApiClient.convertToType(data['email'], 'String');
            }
            if (data.hasOwnProperty('token')) {
                obj['token'] = ApiClient.convertToType(data['token'], 'String');
            }
            if (data.hasOwnProperty('password')) {
                obj['password'] = ApiClient.convertToType(data['password'], 'String');
            }
        }
        return obj;
    }

/**
     * @return {String}
     */
    getEmail() {
        return this.email;
    }

    /**
     * @param {String} email
     */
    setEmail(email) {
        this['email'] = email;
    }
/**
     * @return {String}
     */
    getToken() {
        return this.token;
    }

    /**
     * @param {String} token
     */
    setToken(token) {
        this['token'] = token;
    }
/**
     * @return {String}
     */
    getPassword() {
        return this.password;
    }

    /**
     * @param {String} password
     */
    setPassword(password) {
        this['password'] = password;
    }

}

/**
 * @member {String} email
 */
ResetPasswordPackage.prototype['email'] = undefined;

/**
 * @member {String} token
 */
ResetPasswordPackage.prototype['token'] = undefined;

/**
 * @member {String} password
 */
ResetPasswordPackage.prototype['password'] = undefined;






export default ResetPasswordPackage;

