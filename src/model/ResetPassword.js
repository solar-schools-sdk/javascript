/**
 * Solar Schools - Public API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * Contact: dev@solarschools.net
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The ResetPassword model module.
 * @module model/ResetPassword
 * @version 0.0.0
 */
class ResetPassword {
    /**
     * Constructs a new <code>ResetPassword</code>.
     * @alias module:model/ResetPassword
     * @param email {String} 
     * @param token {String} 
     * @param password {String} 
     */
    constructor(email, token, password) { 
        
        ResetPassword.initialize(this, email, token, password);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, email, token, password) { 
        obj['email'] = email;
        obj['token'] = token;
        obj['password'] = password;
    }

    /**
     * Constructs a <code>ResetPassword</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ResetPassword} obj Optional instance to populate.
     * @return {module:model/ResetPassword} The populated <code>ResetPassword</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new ResetPassword();

            if (data.hasOwnProperty('email')) {
                obj['email'] = ApiClient.convertToType(data['email'], 'String');
            }
            if (data.hasOwnProperty('token')) {
                obj['token'] = ApiClient.convertToType(data['token'], 'String');
            }
            if (data.hasOwnProperty('password')) {
                obj['password'] = ApiClient.convertToType(data['password'], 'String');
            }
            if (data.hasOwnProperty('resource')) {
                obj['resource'] = ApiClient.convertToType(data['resource'], 'String');
            }
        }
        return obj;
    }

    /**
     * Validates the JSON data with respect to <code>ResetPassword</code>.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @return {boolean} to indicate whether the JSON data is valid with respect to <code>ResetPassword</code>.
     */
    static validateJSON(data) {
        // check to make sure all required properties are present in the JSON string
        for (const property of ResetPassword.RequiredProperties) {
            if (!data[property]) {
                throw new Error("The required field `" + property + "` is not found in the JSON data: " + JSON.stringify(data));
            }
        }
        // ensure the json data is a string
        if (data['email'] && !(typeof data['email'] === 'string' || data['email'] instanceof String)) {
            throw new Error("Expected the field `email` to be a primitive type in the JSON string but got " + data['email']);
        }
        // ensure the json data is a string
        if (data['token'] && !(typeof data['token'] === 'string' || data['token'] instanceof String)) {
            throw new Error("Expected the field `token` to be a primitive type in the JSON string but got " + data['token']);
        }
        // ensure the json data is a string
        if (data['password'] && !(typeof data['password'] === 'string' || data['password'] instanceof String)) {
            throw new Error("Expected the field `password` to be a primitive type in the JSON string but got " + data['password']);
        }
        // ensure the json data is a string
        if (data['resource'] && !(typeof data['resource'] === 'string' || data['resource'] instanceof String)) {
            throw new Error("Expected the field `resource` to be a primitive type in the JSON string but got " + data['resource']);
        }

        return true;
    }

/**
     * @return {String}
     */
    getEmail() {
        return this.email;
    }

    /**
     * @param {String} email
     */
    setEmail(email) {
        this['email'] = email;
    }
/**
     * @return {String}
     */
    getToken() {
        return this.token;
    }

    /**
     * @param {String} token
     */
    setToken(token) {
        this['token'] = token;
    }
/**
     * @return {String}
     */
    getPassword() {
        return this.password;
    }

    /**
     * @param {String} password
     */
    setPassword(password) {
        this['password'] = password;
    }
/**
     * @return {String}
     */
    getResource() {
        return this.resource;
    }

    /**
     * @param {String} resource
     */
    setResource(resource) {
        this['resource'] = resource;
    }

}

ResetPassword.RequiredProperties = ["email", "token", "password"];

/**
 * @member {String} email
 */
ResetPassword.prototype['email'] = undefined;

/**
 * @member {String} token
 */
ResetPassword.prototype['token'] = undefined;

/**
 * @member {String} password
 */
ResetPassword.prototype['password'] = undefined;

/**
 * @member {String} resource
 */
ResetPassword.prototype['resource'] = undefined;






export default ResetPassword;

