/**
 * Solar Schools - Public API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * Contact: dev@solarschools.net
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The VirtualMeterChannel model module.
 * @module model/VirtualMeterChannel
 * @version 0.0.0
 */
class VirtualMeterChannel {
    /**
     * Constructs a new <code>VirtualMeterChannel</code>.
     * @alias module:model/VirtualMeterChannel
     * @param channelId {Number} 
     * @param multiplier {Number} 
     */
    constructor(channelId, multiplier) { 
        
        VirtualMeterChannel.initialize(this, channelId, multiplier);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, channelId, multiplier) { 
        obj['channelId'] = channelId;
        obj['multiplier'] = multiplier;
    }

    /**
     * Constructs a <code>VirtualMeterChannel</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/VirtualMeterChannel} obj Optional instance to populate.
     * @return {module:model/VirtualMeterChannel} The populated <code>VirtualMeterChannel</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new VirtualMeterChannel();

            if (data.hasOwnProperty('channelId')) {
                obj['channelId'] = ApiClient.convertToType(data['channelId'], 'Number');
            }
            if (data.hasOwnProperty('multiplier')) {
                obj['multiplier'] = ApiClient.convertToType(data['multiplier'], 'Number');
            }
        }
        return obj;
    }

    /**
     * Validates the JSON data with respect to <code>VirtualMeterChannel</code>.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @return {boolean} to indicate whether the JSON data is valid with respect to <code>VirtualMeterChannel</code>.
     */
    static validateJSON(data) {
        // check to make sure all required properties are present in the JSON string
        for (const property of VirtualMeterChannel.RequiredProperties) {
            if (!data[property]) {
                throw new Error("The required field `" + property + "` is not found in the JSON data: " + JSON.stringify(data));
            }
        }

        return true;
    }

/**
     * @return {Number}
     */
    getChannelId() {
        return this.channelId;
    }

    /**
     * @param {Number} channelId
     */
    setChannelId(channelId) {
        this['channelId'] = channelId;
    }
/**
     * @return {Number}
     */
    getMultiplier() {
        return this.multiplier;
    }

    /**
     * @param {Number} multiplier
     */
    setMultiplier(multiplier) {
        this['multiplier'] = multiplier;
    }

}

VirtualMeterChannel.RequiredProperties = ["channelId", "multiplier"];

/**
 * @member {Number} channelId
 */
VirtualMeterChannel.prototype['channelId'] = undefined;

/**
 * @member {Number} multiplier
 */
VirtualMeterChannel.prototype['multiplier'] = undefined;






export default VirtualMeterChannel;

