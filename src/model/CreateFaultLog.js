/**
 * Solar Schools - Public API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * Contact: dev@solarschools.net
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The CreateFaultLog model module.
 * @module model/CreateFaultLog
 * @version 0.0.0
 */
class CreateFaultLog {
    /**
     * Constructs a new <code>CreateFaultLog</code>.
     * @alias module:model/CreateFaultLog
     * @param note {String} 
     */
    constructor(note) { 
        
        CreateFaultLog.initialize(this, note);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, note) { 
        obj['note'] = note;
    }

    /**
     * Constructs a <code>CreateFaultLog</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateFaultLog} obj Optional instance to populate.
     * @return {module:model/CreateFaultLog} The populated <code>CreateFaultLog</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new CreateFaultLog();

            if (data.hasOwnProperty('severity')) {
                obj['severity'] = ApiClient.convertToType(data['severity'], 'Number');
            }
            if (data.hasOwnProperty('status')) {
                obj['status'] = ApiClient.convertToType(data['status'], 'Number');
            }
            if (data.hasOwnProperty('note')) {
                obj['note'] = ApiClient.convertToType(data['note'], 'String');
            }
            if (data.hasOwnProperty('duration')) {
                obj['duration'] = ApiClient.convertToType(data['duration'], 'Number');
            }
            if (data.hasOwnProperty('logTime')) {
                obj['logTime'] = ApiClient.convertToType(data['logTime'], 'String');
            }
        }
        return obj;
    }

    /**
     * Validates the JSON data with respect to <code>CreateFaultLog</code>.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @return {boolean} to indicate whether the JSON data is valid with respect to <code>CreateFaultLog</code>.
     */
    static validateJSON(data) {
        // check to make sure all required properties are present in the JSON string
        for (const property of CreateFaultLog.RequiredProperties) {
            if (!data[property]) {
                throw new Error("The required field `" + property + "` is not found in the JSON data: " + JSON.stringify(data));
            }
        }
        // ensure the json data is a string
        if (data['note'] && !(typeof data['note'] === 'string' || data['note'] instanceof String)) {
            throw new Error("Expected the field `note` to be a primitive type in the JSON string but got " + data['note']);
        }
        // ensure the json data is a string
        if (data['logTime'] && !(typeof data['logTime'] === 'string' || data['logTime'] instanceof String)) {
            throw new Error("Expected the field `logTime` to be a primitive type in the JSON string but got " + data['logTime']);
        }

        return true;
    }

/**
     * Returns Change in severity of the fault.
     * @return {Number}
     */
    getSeverity() {
        return this.severity;
    }

    /**
     * Sets Change in severity of the fault.
     * @param {Number} severity Change in severity of the fault.
     */
    setSeverity(severity) {
        this['severity'] = severity;
    }
/**
     * Returns Change in status of the fault.
     * @return {Number}
     */
    getStatus() {
        return this.status;
    }

    /**
     * Sets Change in status of the fault.
     * @param {Number} status Change in status of the fault.
     */
    setStatus(status) {
        this['status'] = status;
    }
/**
     * @return {String}
     */
    getNote() {
        return this.note;
    }

    /**
     * @param {String} note
     */
    setNote(note) {
        this['note'] = note;
    }
/**
     * Returns Duration in minutes spent on this log.
     * @return {Number}
     */
    getDuration() {
        return this.duration;
    }

    /**
     * Sets Duration in minutes spent on this log.
     * @param {Number} duration Duration in minutes spent on this log.
     */
    setDuration(duration) {
        this['duration'] = duration;
    }
/**
     * Returns Time of the log in the site's timezone.
     * @return {String}
     */
    getLogTime() {
        return this.logTime;
    }

    /**
     * Sets Time of the log in the site's timezone.
     * @param {String} logTime Time of the log in the site's timezone.
     */
    setLogTime(logTime) {
        this['logTime'] = logTime;
    }

}

CreateFaultLog.RequiredProperties = ["note"];

/**
 * Change in severity of the fault.
 * @member {Number} severity
 */
CreateFaultLog.prototype['severity'] = undefined;

/**
 * Change in status of the fault.
 * @member {Number} status
 */
CreateFaultLog.prototype['status'] = undefined;

/**
 * @member {String} note
 */
CreateFaultLog.prototype['note'] = undefined;

/**
 * Duration in minutes spent on this log.
 * @member {Number} duration
 */
CreateFaultLog.prototype['duration'] = undefined;

/**
 * Time of the log in the site's timezone.
 * @member {String} logTime
 */
CreateFaultLog.prototype['logTime'] = undefined;






export default CreateFaultLog;

