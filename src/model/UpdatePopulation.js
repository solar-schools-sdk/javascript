/**
 * Solar Schools - Public API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * Contact: dev@solarschools.net
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The UpdatePopulation model module.
 * @module model/UpdatePopulation
 * @version 0.0.0
 */
class UpdatePopulation {
    /**
     * Constructs a new <code>UpdatePopulation</code>.
     * @alias module:model/UpdatePopulation
     * @param startDate {String} Inclusive start date of the population in the site's timezone.
     * @param endDate {String} Exclusive end date of the population in the site's timezone.
     * @param personCount {Number} Person count at the site within the time range.
     */
    constructor(startDate, endDate, personCount) { 
        
        UpdatePopulation.initialize(this, startDate, endDate, personCount);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, startDate, endDate, personCount) { 
        obj['startDate'] = startDate;
        obj['endDate'] = endDate;
        obj['personCount'] = personCount;
    }

    /**
     * Constructs a <code>UpdatePopulation</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UpdatePopulation} obj Optional instance to populate.
     * @return {module:model/UpdatePopulation} The populated <code>UpdatePopulation</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new UpdatePopulation();

            if (data.hasOwnProperty('startDate')) {
                obj['startDate'] = ApiClient.convertToType(data['startDate'], 'String');
            }
            if (data.hasOwnProperty('endDate')) {
                obj['endDate'] = ApiClient.convertToType(data['endDate'], 'String');
            }
            if (data.hasOwnProperty('personCount')) {
                obj['personCount'] = ApiClient.convertToType(data['personCount'], 'Number');
            }
        }
        return obj;
    }

    /**
     * Validates the JSON data with respect to <code>UpdatePopulation</code>.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @return {boolean} to indicate whether the JSON data is valid with respect to <code>UpdatePopulation</code>.
     */
    static validateJSON(data) {
        // check to make sure all required properties are present in the JSON string
        for (const property of UpdatePopulation.RequiredProperties) {
            if (!data[property]) {
                throw new Error("The required field `" + property + "` is not found in the JSON data: " + JSON.stringify(data));
            }
        }
        // ensure the json data is a string
        if (data['startDate'] && !(typeof data['startDate'] === 'string' || data['startDate'] instanceof String)) {
            throw new Error("Expected the field `startDate` to be a primitive type in the JSON string but got " + data['startDate']);
        }
        // ensure the json data is a string
        if (data['endDate'] && !(typeof data['endDate'] === 'string' || data['endDate'] instanceof String)) {
            throw new Error("Expected the field `endDate` to be a primitive type in the JSON string but got " + data['endDate']);
        }

        return true;
    }

/**
     * Returns Inclusive start date of the population in the site's timezone.
     * @return {String}
     */
    getStartDate() {
        return this.startDate;
    }

    /**
     * Sets Inclusive start date of the population in the site's timezone.
     * @param {String} startDate Inclusive start date of the population in the site's timezone.
     */
    setStartDate(startDate) {
        this['startDate'] = startDate;
    }
/**
     * Returns Exclusive end date of the population in the site's timezone.
     * @return {String}
     */
    getEndDate() {
        return this.endDate;
    }

    /**
     * Sets Exclusive end date of the population in the site's timezone.
     * @param {String} endDate Exclusive end date of the population in the site's timezone.
     */
    setEndDate(endDate) {
        this['endDate'] = endDate;
    }
/**
     * Returns Person count at the site within the time range.
     * @return {Number}
     */
    getPersonCount() {
        return this.personCount;
    }

    /**
     * Sets Person count at the site within the time range.
     * @param {Number} personCount Person count at the site within the time range.
     */
    setPersonCount(personCount) {
        this['personCount'] = personCount;
    }

}

UpdatePopulation.RequiredProperties = ["startDate", "endDate", "personCount"];

/**
 * Inclusive start date of the population in the site's timezone.
 * @member {String} startDate
 */
UpdatePopulation.prototype['startDate'] = undefined;

/**
 * Exclusive end date of the population in the site's timezone.
 * @member {String} endDate
 */
UpdatePopulation.prototype['endDate'] = undefined;

/**
 * Person count at the site within the time range.
 * @member {Number} personCount
 */
UpdatePopulation.prototype['personCount'] = undefined;






export default UpdatePopulation;

