/**
 * Solar Schools - Public API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 * Contact: dev@solarschools.net.au
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The RestUserPasswordRequestData model module.
 * @module model/RestUserPasswordRequestData
 * @version 0.0.0
 */
class RestUserPasswordRequestData {
    /**
     * Constructs a new <code>RestUserPasswordRequestData</code>.
     * @alias module:model/RestUserPasswordRequestData
     * @param username {String} 
     * @param resetCode {String} 
     * @param newPassword {String} 
     */
    constructor(username, resetCode, newPassword) { 
        
        RestUserPasswordRequestData.initialize(this, username, resetCode, newPassword);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, username, resetCode, newPassword) { 
        obj['username'] = username;
        obj['resetCode'] = resetCode;
        obj['newPassword'] = newPassword;
    }

    /**
     * Constructs a <code>RestUserPasswordRequestData</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/RestUserPasswordRequestData} obj Optional instance to populate.
     * @return {module:model/RestUserPasswordRequestData} The populated <code>RestUserPasswordRequestData</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new RestUserPasswordRequestData();

            if (data.hasOwnProperty('username')) {
                obj['username'] = ApiClient.convertToType(data['username'], 'String');
            }
            if (data.hasOwnProperty('resetCode')) {
                obj['resetCode'] = ApiClient.convertToType(data['resetCode'], 'String');
            }
            if (data.hasOwnProperty('newPassword')) {
                obj['newPassword'] = ApiClient.convertToType(data['newPassword'], 'String');
            }
        }
        return obj;
    }

/**
     * @return {String}
     */
    getUsername() {
        return this.username;
    }

    /**
     * @param {String} username
     */
    setUsername(username) {
        this['username'] = username;
    }
/**
     * @return {String}
     */
    getResetCode() {
        return this.resetCode;
    }

    /**
     * @param {String} resetCode
     */
    setResetCode(resetCode) {
        this['resetCode'] = resetCode;
    }
/**
     * @return {String}
     */
    getNewPassword() {
        return this.newPassword;
    }

    /**
     * @param {String} newPassword
     */
    setNewPassword(newPassword) {
        this['newPassword'] = newPassword;
    }

}

/**
 * @member {String} username
 */
RestUserPasswordRequestData.prototype['username'] = undefined;

/**
 * @member {String} resetCode
 */
RestUserPasswordRequestData.prototype['resetCode'] = undefined;

/**
 * @member {String} newPassword
 */
RestUserPasswordRequestData.prototype['newPassword'] = undefined;






export default RestUserPasswordRequestData;

