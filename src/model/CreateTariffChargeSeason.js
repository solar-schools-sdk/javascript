/**
 * Solar Schools - Public API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * Contact: dev@solarschools.net
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The CreateTariffChargeSeason model module.
 * @module model/CreateTariffChargeSeason
 * @version 0.0.0
 */
class CreateTariffChargeSeason {
    /**
     * Constructs a new <code>CreateTariffChargeSeason</code>.
     * @alias module:model/CreateTariffChargeSeason
     * @param monthStart {Number} Numerical inclusive start month of the season; 1-12.
     * @param monthEnd {Number} Numerical exclusive end month of the season; 1-12.
     * @param unitPrice {Number} Price of the tariff in the tariff's unit for this season. Overrides the base charge value.
     * @param unitDiscount {Number} Discount amount to apply to the quantity before price calculation. Overrides the base charge value.
     */
    constructor(monthStart, monthEnd, unitPrice, unitDiscount) { 
        
        CreateTariffChargeSeason.initialize(this, monthStart, monthEnd, unitPrice, unitDiscount);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, monthStart, monthEnd, unitPrice, unitDiscount) { 
        obj['monthStart'] = monthStart;
        obj['monthEnd'] = monthEnd;
        obj['unitPrice'] = unitPrice;
        obj['unitDiscount'] = unitDiscount;
    }

    /**
     * Constructs a <code>CreateTariffChargeSeason</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateTariffChargeSeason} obj Optional instance to populate.
     * @return {module:model/CreateTariffChargeSeason} The populated <code>CreateTariffChargeSeason</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new CreateTariffChargeSeason();

            if (data.hasOwnProperty('monthStart')) {
                obj['monthStart'] = ApiClient.convertToType(data['monthStart'], 'Number');
            }
            if (data.hasOwnProperty('monthEnd')) {
                obj['monthEnd'] = ApiClient.convertToType(data['monthEnd'], 'Number');
            }
            if (data.hasOwnProperty('unitPrice')) {
                obj['unitPrice'] = ApiClient.convertToType(data['unitPrice'], 'Number');
            }
            if (data.hasOwnProperty('unitDiscount')) {
                obj['unitDiscount'] = ApiClient.convertToType(data['unitDiscount'], 'Number');
            }
        }
        return obj;
    }

    /**
     * Validates the JSON data with respect to <code>CreateTariffChargeSeason</code>.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @return {boolean} to indicate whether the JSON data is valid with respect to <code>CreateTariffChargeSeason</code>.
     */
    static validateJSON(data) {
        // check to make sure all required properties are present in the JSON string
        for (const property of CreateTariffChargeSeason.RequiredProperties) {
            if (!data[property]) {
                throw new Error("The required field `" + property + "` is not found in the JSON data: " + JSON.stringify(data));
            }
        }

        return true;
    }

/**
     * Returns Numerical inclusive start month of the season; 1-12.
     * @return {Number}
     */
    getMonthStart() {
        return this.monthStart;
    }

    /**
     * Sets Numerical inclusive start month of the season; 1-12.
     * @param {Number} monthStart Numerical inclusive start month of the season; 1-12.
     */
    setMonthStart(monthStart) {
        this['monthStart'] = monthStart;
    }
/**
     * Returns Numerical exclusive end month of the season; 1-12.
     * @return {Number}
     */
    getMonthEnd() {
        return this.monthEnd;
    }

    /**
     * Sets Numerical exclusive end month of the season; 1-12.
     * @param {Number} monthEnd Numerical exclusive end month of the season; 1-12.
     */
    setMonthEnd(monthEnd) {
        this['monthEnd'] = monthEnd;
    }
/**
     * Returns Price of the tariff in the tariff's unit for this season. Overrides the base charge value.
     * @return {Number}
     */
    getUnitPrice() {
        return this.unitPrice;
    }

    /**
     * Sets Price of the tariff in the tariff's unit for this season. Overrides the base charge value.
     * @param {Number} unitPrice Price of the tariff in the tariff's unit for this season. Overrides the base charge value.
     */
    setUnitPrice(unitPrice) {
        this['unitPrice'] = unitPrice;
    }
/**
     * Returns Discount amount to apply to the quantity before price calculation. Overrides the base charge value.
     * @return {Number}
     */
    getUnitDiscount() {
        return this.unitDiscount;
    }

    /**
     * Sets Discount amount to apply to the quantity before price calculation. Overrides the base charge value.
     * @param {Number} unitDiscount Discount amount to apply to the quantity before price calculation. Overrides the base charge value.
     */
    setUnitDiscount(unitDiscount) {
        this['unitDiscount'] = unitDiscount;
    }

}

CreateTariffChargeSeason.RequiredProperties = ["monthStart", "monthEnd", "unitPrice", "unitDiscount"];

/**
 * Numerical inclusive start month of the season; 1-12.
 * @member {Number} monthStart
 */
CreateTariffChargeSeason.prototype['monthStart'] = undefined;

/**
 * Numerical exclusive end month of the season; 1-12.
 * @member {Number} monthEnd
 */
CreateTariffChargeSeason.prototype['monthEnd'] = undefined;

/**
 * Price of the tariff in the tariff's unit for this season. Overrides the base charge value.
 * @member {Number} unitPrice
 */
CreateTariffChargeSeason.prototype['unitPrice'] = undefined;

/**
 * Discount amount to apply to the quantity before price calculation. Overrides the base charge value.
 * @member {Number} unitDiscount
 */
CreateTariffChargeSeason.prototype['unitDiscount'] = undefined;






export default CreateTariffChargeSeason;

