/**
 * Solar Schools - Public API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 * Contact: dev@solarschools.net.au
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The UpdateUserRequest model module.
 * @module model/UpdateUserRequest
 * @version 0.0.0
 */
class UpdateUserRequest {
    /**
     * Constructs a new <code>UpdateUserRequest</code>.
     * @alias module:model/UpdateUserRequest
     */
    constructor() { 
        
        UpdateUserRequest.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>UpdateUserRequest</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/UpdateUserRequest} obj Optional instance to populate.
     * @return {module:model/UpdateUserRequest} The populated <code>UpdateUserRequest</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new UpdateUserRequest();

            if (data.hasOwnProperty('role')) {
                obj['role'] = ApiClient.convertToType(data['role'], 'String');
            }
            if (data.hasOwnProperty('first_name')) {
                obj['first_name'] = ApiClient.convertToType(data['first_name'], 'String');
            }
            if (data.hasOwnProperty('surname')) {
                obj['surname'] = ApiClient.convertToType(data['surname'], 'String');
            }
            if (data.hasOwnProperty('phone')) {
                obj['phone'] = ApiClient.convertToType(data['phone'], 'String');
            }
            if (data.hasOwnProperty('email')) {
                obj['email'] = ApiClient.convertToType(data['email'], 'String');
            }
            if (data.hasOwnProperty('street')) {
                obj['street'] = ApiClient.convertToType(data['street'], 'String');
            }
            if (data.hasOwnProperty('suburb')) {
                obj['suburb'] = ApiClient.convertToType(data['suburb'], 'String');
            }
            if (data.hasOwnProperty('city')) {
                obj['city'] = ApiClient.convertToType(data['city'], 'String');
            }
            if (data.hasOwnProperty('state')) {
                obj['state'] = ApiClient.convertToType(data['state'], 'String');
            }
            if (data.hasOwnProperty('postcode')) {
                obj['postcode'] = ApiClient.convertToType(data['postcode'], 'String');
            }
            if (data.hasOwnProperty('country')) {
                obj['country'] = ApiClient.convertToType(data['country'], 'String');
            }
        }
        return obj;
    }

/**
     * @return {String}
     */
    getRole() {
        return this.role;
    }

    /**
     * @param {String} role
     */
    setRole(role) {
        this['role'] = role;
    }
/**
     * @return {String}
     */
    getFirstName() {
        return this.first_name;
    }

    /**
     * @param {String} firstName
     */
    setFirstName(firstName) {
        this['first_name'] = firstName;
    }
/**
     * @return {String}
     */
    getSurname() {
        return this.surname;
    }

    /**
     * @param {String} surname
     */
    setSurname(surname) {
        this['surname'] = surname;
    }
/**
     * @return {String}
     */
    getPhone() {
        return this.phone;
    }

    /**
     * @param {String} phone
     */
    setPhone(phone) {
        this['phone'] = phone;
    }
/**
     * @return {String}
     */
    getEmail() {
        return this.email;
    }

    /**
     * @param {String} email
     */
    setEmail(email) {
        this['email'] = email;
    }
/**
     * @return {String}
     */
    getStreet() {
        return this.street;
    }

    /**
     * @param {String} street
     */
    setStreet(street) {
        this['street'] = street;
    }
/**
     * @return {String}
     */
    getSuburb() {
        return this.suburb;
    }

    /**
     * @param {String} suburb
     */
    setSuburb(suburb) {
        this['suburb'] = suburb;
    }
/**
     * @return {String}
     */
    getCity() {
        return this.city;
    }

    /**
     * @param {String} city
     */
    setCity(city) {
        this['city'] = city;
    }
/**
     * @return {String}
     */
    getState() {
        return this.state;
    }

    /**
     * @param {String} state
     */
    setState(state) {
        this['state'] = state;
    }
/**
     * @return {String}
     */
    getPostcode() {
        return this.postcode;
    }

    /**
     * @param {String} postcode
     */
    setPostcode(postcode) {
        this['postcode'] = postcode;
    }
/**
     * @return {String}
     */
    getCountry() {
        return this.country;
    }

    /**
     * @param {String} country
     */
    setCountry(country) {
        this['country'] = country;
    }

}

/**
 * @member {String} role
 */
UpdateUserRequest.prototype['role'] = undefined;

/**
 * @member {String} first_name
 */
UpdateUserRequest.prototype['first_name'] = undefined;

/**
 * @member {String} surname
 */
UpdateUserRequest.prototype['surname'] = undefined;

/**
 * @member {String} phone
 */
UpdateUserRequest.prototype['phone'] = undefined;

/**
 * @member {String} email
 */
UpdateUserRequest.prototype['email'] = undefined;

/**
 * @member {String} street
 */
UpdateUserRequest.prototype['street'] = undefined;

/**
 * @member {String} suburb
 */
UpdateUserRequest.prototype['suburb'] = undefined;

/**
 * @member {String} city
 */
UpdateUserRequest.prototype['city'] = undefined;

/**
 * @member {String} state
 */
UpdateUserRequest.prototype['state'] = undefined;

/**
 * @member {String} postcode
 */
UpdateUserRequest.prototype['postcode'] = undefined;

/**
 * @member {String} country
 */
UpdateUserRequest.prototype['country'] = undefined;






export default UpdateUserRequest;

