/**
 * Solar Schools - Public API
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * Contact: dev@solarschools.net
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import CreateSitePopulation from './CreateSitePopulation';

/**
 * The CreateSite model module.
 * @module model/CreateSite
 * @version 0.0.0
 */
class CreateSite {
    /**
     * Constructs a new <code>CreateSite</code>.
     * @alias module:model/CreateSite
     * @param organisationId {Number} 
     * @param street {String} 
     * @param city {String} 
     * @param countryDivisionId {Number} 
     * @param latitude {Number} 
     * @param longitude {Number} 
     * @param status {Number} 
     * @param population {Array.<module:model/CreateSitePopulation>} 
     */
    constructor(organisationId, street, city, countryDivisionId, latitude, longitude, status, population) { 
        
        CreateSite.initialize(this, organisationId, street, city, countryDivisionId, latitude, longitude, status, population);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj, organisationId, street, city, countryDivisionId, latitude, longitude, status, population) { 
        obj['organisationId'] = organisationId;
        obj['street'] = street;
        obj['city'] = city;
        obj['countryDivisionId'] = countryDivisionId;
        obj['latitude'] = latitude;
        obj['longitude'] = longitude;
        obj['status'] = status;
        obj['population'] = population;
    }

    /**
     * Constructs a <code>CreateSite</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CreateSite} obj Optional instance to populate.
     * @return {module:model/CreateSite} The populated <code>CreateSite</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new CreateSite();

            if (data.hasOwnProperty('organisationId')) {
                obj['organisationId'] = ApiClient.convertToType(data['organisationId'], 'Number');
            }
            if (data.hasOwnProperty('name')) {
                obj['name'] = ApiClient.convertToType(data['name'], 'String');
            }
            if (data.hasOwnProperty('slug')) {
                obj['slug'] = ApiClient.convertToType(data['slug'], 'String');
            }
            if (data.hasOwnProperty('description')) {
                obj['description'] = ApiClient.convertToType(data['description'], 'String');
            }
            if (data.hasOwnProperty('street')) {
                obj['street'] = ApiClient.convertToType(data['street'], 'String');
            }
            if (data.hasOwnProperty('suburb')) {
                obj['suburb'] = ApiClient.convertToType(data['suburb'], 'String');
            }
            if (data.hasOwnProperty('city')) {
                obj['city'] = ApiClient.convertToType(data['city'], 'String');
            }
            if (data.hasOwnProperty('region')) {
                obj['region'] = ApiClient.convertToType(data['region'], 'String');
            }
            if (data.hasOwnProperty('postcode')) {
                obj['postcode'] = ApiClient.convertToType(data['postcode'], 'String');
            }
            if (data.hasOwnProperty('countryDivisionId')) {
                obj['countryDivisionId'] = ApiClient.convertToType(data['countryDivisionId'], 'Number');
            }
            if (data.hasOwnProperty('latitude')) {
                obj['latitude'] = ApiClient.convertToType(data['latitude'], 'Number');
            }
            if (data.hasOwnProperty('longitude')) {
                obj['longitude'] = ApiClient.convertToType(data['longitude'], 'Number');
            }
            if (data.hasOwnProperty('weatherLocationId')) {
                obj['weatherLocationId'] = ApiClient.convertToType(data['weatherLocationId'], 'Number');
            }
            if (data.hasOwnProperty('hasGridData')) {
                obj['hasGridData'] = ApiClient.convertToType(data['hasGridData'], 'Number');
            }
            if (data.hasOwnProperty('hasSolarData')) {
                obj['hasSolarData'] = ApiClient.convertToType(data['hasSolarData'], 'Number');
            }
            if (data.hasOwnProperty('status')) {
                obj['status'] = ApiClient.convertToType(data['status'], 'Number');
            }
            if (data.hasOwnProperty('population')) {
                obj['population'] = ApiClient.convertToType(data['population'], [CreateSitePopulation]);
            }
            if (data.hasOwnProperty('customAttributes')) {
                obj['customAttributes'] = ApiClient.convertToType(data['customAttributes'], {'String': 'String'});
            }
        }
        return obj;
    }

    /**
     * Validates the JSON data with respect to <code>CreateSite</code>.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @return {boolean} to indicate whether the JSON data is valid with respect to <code>CreateSite</code>.
     */
    static validateJSON(data) {
        // check to make sure all required properties are present in the JSON string
        for (const property of CreateSite.RequiredProperties) {
            if (!data[property]) {
                throw new Error("The required field `" + property + "` is not found in the JSON data: " + JSON.stringify(data));
            }
        }
        // ensure the json data is a string
        if (data['name'] && !(typeof data['name'] === 'string' || data['name'] instanceof String)) {
            throw new Error("Expected the field `name` to be a primitive type in the JSON string but got " + data['name']);
        }
        // ensure the json data is a string
        if (data['slug'] && !(typeof data['slug'] === 'string' || data['slug'] instanceof String)) {
            throw new Error("Expected the field `slug` to be a primitive type in the JSON string but got " + data['slug']);
        }
        // ensure the json data is a string
        if (data['description'] && !(typeof data['description'] === 'string' || data['description'] instanceof String)) {
            throw new Error("Expected the field `description` to be a primitive type in the JSON string but got " + data['description']);
        }
        // ensure the json data is a string
        if (data['street'] && !(typeof data['street'] === 'string' || data['street'] instanceof String)) {
            throw new Error("Expected the field `street` to be a primitive type in the JSON string but got " + data['street']);
        }
        // ensure the json data is a string
        if (data['suburb'] && !(typeof data['suburb'] === 'string' || data['suburb'] instanceof String)) {
            throw new Error("Expected the field `suburb` to be a primitive type in the JSON string but got " + data['suburb']);
        }
        // ensure the json data is a string
        if (data['city'] && !(typeof data['city'] === 'string' || data['city'] instanceof String)) {
            throw new Error("Expected the field `city` to be a primitive type in the JSON string but got " + data['city']);
        }
        // ensure the json data is a string
        if (data['region'] && !(typeof data['region'] === 'string' || data['region'] instanceof String)) {
            throw new Error("Expected the field `region` to be a primitive type in the JSON string but got " + data['region']);
        }
        // ensure the json data is a string
        if (data['postcode'] && !(typeof data['postcode'] === 'string' || data['postcode'] instanceof String)) {
            throw new Error("Expected the field `postcode` to be a primitive type in the JSON string but got " + data['postcode']);
        }
        if (data['population']) { // data not null
            // ensure the json data is an array
            if (!Array.isArray(data['population'])) {
                throw new Error("Expected the field `population` to be an array in the JSON data but got " + data['population']);
            }
            // validate the optional field `population` (array)
            for (const item of data['population']) {
                CreateSitePopulation.validateJsonObject(item);
            };
        }

        return true;
    }

/**
     * @return {Number}
     */
    getOrganisationId() {
        return this.organisationId;
    }

    /**
     * @param {Number} organisationId
     */
    setOrganisationId(organisationId) {
        this['organisationId'] = organisationId;
    }
/**
     * @return {String}
     */
    getName() {
        return this.name;
    }

    /**
     * @param {String} name
     */
    setName(name) {
        this['name'] = name;
    }
/**
     * @return {String}
     */
    getSlug() {
        return this.slug;
    }

    /**
     * @param {String} slug
     */
    setSlug(slug) {
        this['slug'] = slug;
    }
/**
     * @return {String}
     */
    getDescription() {
        return this.description;
    }

    /**
     * @param {String} description
     */
    setDescription(description) {
        this['description'] = description;
    }
/**
     * @return {String}
     */
    getStreet() {
        return this.street;
    }

    /**
     * @param {String} street
     */
    setStreet(street) {
        this['street'] = street;
    }
/**
     * @return {String}
     */
    getSuburb() {
        return this.suburb;
    }

    /**
     * @param {String} suburb
     */
    setSuburb(suburb) {
        this['suburb'] = suburb;
    }
/**
     * @return {String}
     */
    getCity() {
        return this.city;
    }

    /**
     * @param {String} city
     */
    setCity(city) {
        this['city'] = city;
    }
/**
     * @return {String}
     */
    getRegion() {
        return this.region;
    }

    /**
     * @param {String} region
     */
    setRegion(region) {
        this['region'] = region;
    }
/**
     * @return {String}
     */
    getPostcode() {
        return this.postcode;
    }

    /**
     * @param {String} postcode
     */
    setPostcode(postcode) {
        this['postcode'] = postcode;
    }
/**
     * @return {Number}
     */
    getCountryDivisionId() {
        return this.countryDivisionId;
    }

    /**
     * @param {Number} countryDivisionId
     */
    setCountryDivisionId(countryDivisionId) {
        this['countryDivisionId'] = countryDivisionId;
    }
/**
     * @return {Number}
     */
    getLatitude() {
        return this.latitude;
    }

    /**
     * @param {Number} latitude
     */
    setLatitude(latitude) {
        this['latitude'] = latitude;
    }
/**
     * @return {Number}
     */
    getLongitude() {
        return this.longitude;
    }

    /**
     * @param {Number} longitude
     */
    setLongitude(longitude) {
        this['longitude'] = longitude;
    }
/**
     * @return {Number}
     */
    getWeatherLocationId() {
        return this.weatherLocationId;
    }

    /**
     * @param {Number} weatherLocationId
     */
    setWeatherLocationId(weatherLocationId) {
        this['weatherLocationId'] = weatherLocationId;
    }
/**
     * @return {Number}
     */
    getHasGridData() {
        return this.hasGridData;
    }

    /**
     * @param {Number} hasGridData
     */
    setHasGridData(hasGridData) {
        this['hasGridData'] = hasGridData;
    }
/**
     * @return {Number}
     */
    getHasSolarData() {
        return this.hasSolarData;
    }

    /**
     * @param {Number} hasSolarData
     */
    setHasSolarData(hasSolarData) {
        this['hasSolarData'] = hasSolarData;
    }
/**
     * @return {Number}
     */
    getStatus() {
        return this.status;
    }

    /**
     * @param {Number} status
     */
    setStatus(status) {
        this['status'] = status;
    }
/**
     * @return {Array.<module:model/CreateSitePopulation>}
     */
    getPopulation() {
        return this.population;
    }

    /**
     * @param {Array.<module:model/CreateSitePopulation>} population
     */
    setPopulation(population) {
        this['population'] = population;
    }
/**
     * Returns Dynamic collection of custom attributes required for the site.
     * @return {Object.<String, String>}
     */
    getCustomAttributes() {
        return this.customAttributes;
    }

    /**
     * Sets Dynamic collection of custom attributes required for the site.
     * @param {Object.<String, String>} customAttributes Dynamic collection of custom attributes required for the site.
     */
    setCustomAttributes(customAttributes) {
        this['customAttributes'] = customAttributes;
    }

}

CreateSite.RequiredProperties = ["organisationId", "street", "city", "countryDivisionId", "latitude", "longitude", "status", "population"];

/**
 * @member {Number} organisationId
 */
CreateSite.prototype['organisationId'] = undefined;

/**
 * @member {String} name
 */
CreateSite.prototype['name'] = undefined;

/**
 * @member {String} slug
 */
CreateSite.prototype['slug'] = undefined;

/**
 * @member {String} description
 */
CreateSite.prototype['description'] = undefined;

/**
 * @member {String} street
 */
CreateSite.prototype['street'] = undefined;

/**
 * @member {String} suburb
 */
CreateSite.prototype['suburb'] = undefined;

/**
 * @member {String} city
 */
CreateSite.prototype['city'] = undefined;

/**
 * @member {String} region
 */
CreateSite.prototype['region'] = undefined;

/**
 * @member {String} postcode
 */
CreateSite.prototype['postcode'] = undefined;

/**
 * @member {Number} countryDivisionId
 */
CreateSite.prototype['countryDivisionId'] = undefined;

/**
 * @member {Number} latitude
 */
CreateSite.prototype['latitude'] = undefined;

/**
 * @member {Number} longitude
 */
CreateSite.prototype['longitude'] = undefined;

/**
 * @member {Number} weatherLocationId
 */
CreateSite.prototype['weatherLocationId'] = undefined;

/**
 * @member {Number} hasGridData
 */
CreateSite.prototype['hasGridData'] = undefined;

/**
 * @member {Number} hasSolarData
 */
CreateSite.prototype['hasSolarData'] = undefined;

/**
 * @member {Number} status
 */
CreateSite.prototype['status'] = undefined;

/**
 * @member {Array.<module:model/CreateSitePopulation>} population
 */
CreateSite.prototype['population'] = undefined;

/**
 * Dynamic collection of custom attributes required for the site.
 * @member {Object.<String, String>} customAttributes
 */
CreateSite.prototype['customAttributes'] = undefined;






export default CreateSite;

